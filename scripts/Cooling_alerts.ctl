#uses "HVMMGeneral.ctl"

void main()
{
 dyn_string alias;    
  dyn_string allChannels,channelname,channels;
   dyn_string node;
  dyn_dyn_float value;
   
   dyn_dyn_float  mdeg;
   bool isOn;
   
  
   
 allChannels = dpNames("*MDM_*","FwElmbAi");
  for(int i =1; i<=dynlen(allChannels); i++){
   
       dpGet(allChannels[i] +".value",mdeg[i]);
       //mdeg[i]=$temp;
       dpConnect("Alert",allChannels[i] + ".value");
       //DebugN(value);
      
     }
 
}
  
void Alert(string Channeldp,float newValue){ 
   float x,temp;
 string alias = dpGetAlias(Channeldp);
 string recepient;
 float degrees = newValue/1000;
 
 //DebugN("newValue :"+newValue);
 if (((alias == "IPCL1")||(alias == "IPCL2")||(alias == "IPCR1")||/*(alias == "IPCR2")||*/(alias == "HOCL1")||(alias == "HOCL2")||/*(alias == "HOCR1")||*/(alias == "HOCR2"))
   && ((newValue>=31000)&&(newValue<=51000)) ){
    
   //  DebugN(alias);
         alertViaEmail("christos.paraskevopoulos@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
         alertViaSMS("0041754111516", "coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+"  Temp is : " +degrees);
         alertViaSMS("0041754118145", "coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
         alertViaSMS("0041754116441", "coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
//          alertViaSMS("0041754112858", "coolingtemp@cern.ch", "Check_Cooling","Please check cooling on Cosmic Stand at "+alias);
         alertViaEmail("kostas.ntekas@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
         alertViaEmail("theodoros.alexopoulos@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
         alertViaEmail("polyneikis.tzanis@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
         alertViaEmail("maria.perganti@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check cooling on Cosmic Stand at "+alias+" Temp is : " +degrees);
//          alertViaEmail("george.iakovidis@cern.ch","coolingtemp@cern.ch", "Check_Cooling","To Cooling George !!!!");
       
       delay(1800);
 }
    else if (((alias == "IPCL1")||(alias == "IPCL2")||(alias == "IPCR1")||/*(alias == "IPCR2")||*/(alias == "HOCL1")||(alias == "HOCL2")||/*(alias == "HOCR1")||*/(alias == "HOCR2"))
   && (newValue>=100000) ){
    
   //  DebugN(alias);
         alertViaEmail("christos.paraskevopoulos@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
         alertViaSMS("0041754111516", "coolingtemp@cern.ch", "Check_Cooling","Unreliable Readout check T sensor at "+alias);
         alertViaSMS("0041754118145", "coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
         alertViaSMS("0041754116441", "coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
//          alertViaSMS("0041754112858", "coolingtemp@cern.ch", "Check_Cooling","Please check cooling on Cosmic Stand at "+alias);
         alertViaEmail("kostas.ntekas@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
         alertViaEmail("theodoros.alexopoulos@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
         alertViaEmail("polyneikis.tzanis@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
         alertViaEmail("maria.perganti@cern.ch","coolingtemp@cern.ch", "Check_Cooling","Check_Cooling","Possibly unreliable Readout check T sensor at "+alias);
//          alertViaEmail("george.iakovidis@cern.ch","coolingtemp@cern.ch", "Check_Cooling","To Cooling George !!!!");
       
       delay(18000);
 }
}
