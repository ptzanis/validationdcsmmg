#uses "HVMMGeneral.ctl"

void main()
{

   dpConnect("warningFunctionPressure","N1.Gas.Pressure","N2.Gas.Pressure"); 

}


void warningFunctionPressure(string dpSource1, float newValue1, string dpSource2, float newValue2)
{
 
  if(newValue1>6.0 || newValue2>6.0)
  {
   
         alertViaEmail("christos.paraskevopoulos@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         
         alertViaSMS("0041754111516", "pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         alertViaSMS("0041754118145", "pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         alertViaSMS("0041754116441", "pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         
         alertViaEmail("maria.perganti@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         alertViaEmail("kostas.ntekas@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         alertViaEmail("theodoros.alexopoulos@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!"); 
         
         alertViaEmail("polyneikis.tzanis@cern.ch","pressureCosmics@cern.ch", "Pressure Alarm Cosmics@BB5","Warning, IP Pressure: "+ newValue1 +" [mbar] | HO Pressure: "+newValue2+" [mbar] !!!!!!");
         
         delay(30);
   } 
  
  
  
  
}

  
