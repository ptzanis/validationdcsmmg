#include "ReduDrvMan.hxx"

ReduDrvManager::ReduDrvManager() : DrvManager() {
	itsReduHandler = new ReduHandler(this);
}

ReduDrvManager::~ReduDrvManager() {
		delete itsReduHandler;
}

void ReduDrvManager::doReceive(SysMsg& sysMsg ) {
		//Call the original method so that messages are forwarded.
	DrvManager::doReceive( sysMsg );

	// delegate the handling of the message to the hander
	itsReduHandler->doReceive(sysMsg);

}