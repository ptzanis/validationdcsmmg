
LIBS += $$(API_ROOT)/lib.winnt/libCtrl.lib  \
        $$(API_ROOT)/lib.winnt/libComDrv.lib  \
        $$(API_ROOT)/lib.winnt/libManager.lib  \
        $$(API_ROOT)/lib.winnt/libMessages.lib  \
        $$(API_ROOT)/lib.winnt/libDatapoint.lib  \
        $$(API_ROOT)/lib.winnt/libConfigs.lib  \
        $$(API_ROOT)/lib.winnt/libBasics.lib  \
        $$(API_ROOT)/lib.winnt/libV24.lib  \
        $$(API_ROOT)/lib.winnt/bcm.lib

QMAKE_CXXFLAGS += /FI"$${INCL_ROOT}/winnt/win32.h" $(VC8_DEFS) $(D_PVSS_VER)

DEFINES += IS_MSWIN__ _EXCURSION bcm_boolean_h FUNCPROTO _NDEBUG _CONSOLE _WINDLL _UNICODE UNICODE
