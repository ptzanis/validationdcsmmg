PVSS_VERSION_BUILD = 0
# set version also in:
# - Libs/Basics/Utilities/VersInfo.hxx
# - Exe/NTSetup/setup.h
PVSS_VERSION_MAIN = 315

# e.g. for the name of the WebClient/<version>/ directory
PVSS_VERSION_DIR = 3.15

# normalisierte Version zum groesser/kleiner-Vergleich:
# > sh -c ". utils.sh; NormalizedVers x.yy[.zz[.v]]"
PVSS_VERS = 315000
#DLL_VE = V$${PVSS_VERSION_MAIN}_$${PVSS_VERSION_BUILD}$${PVSS_DLL_APP}
#PVSS_DLL_VERS = V$${PVSS_VERSION_MAIN}_$${PVSS_VERSION_BUILD}$${PVSS_DLL_APP}
#PVSS_DLL_VERSMAIN = V$${PVSS_VERSION_MAIN}$${PVSS_DLL_APP}
#PVSS_VERS_DLL=$${PVSS_VERS}.$${PVSS_VERSION_BUILD}

PVSS_DLL_VERS=V315
PVSS_VERS_DLL="\\\"$${PVSS_VERS}\\\""
