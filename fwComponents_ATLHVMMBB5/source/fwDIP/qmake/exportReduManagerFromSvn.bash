#!/bin/bash
cd ../Sources
svn export https://svn.cern.ch/reps/en-ice-svn/trunk/tools/WinccOaComponents/Frameworks/jcop-framework/Utilities/ReduManager/src/main/cpp/ReduMan.cxx
svn export https://svn.cern.ch/reps/en-ice-svn/trunk/tools/WinccOaComponents/Frameworks/jcop-framework/Utilities/ReduManager/src/main/include/ReduMan.hxx
echo "Exported ReduMan (redundancy manager) implementation for compilation"
