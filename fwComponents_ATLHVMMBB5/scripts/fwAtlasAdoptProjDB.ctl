#uses "fwInstallationDBAgent.ctl"
#uses "fwInstallationDB.ctl"
#uses "fwConfigurationDBSystemInformation/fwConfigurationDBSystemInformation.ctl"

main(string computerName,string project)
{
  string database,username,password,owner;
  int  restartProject = 0;
  DebugTN(computerName,project);
  dpGet("fwInstallation_agentParametrization.db.connection.server", database,
    "fwInstallation_agentParametrization.db.connection.username", username,
    "fwInstallation_agentParametrization.db.connection.schemaOwner", owner,
    "fwInstallation_agentParametrization.db.connection.password", password); 
  DebugTN("Connection ",database,username); 
 
  if(database != "" && username != "" && password != ""){
    int con = fwInstallationDB_openDBConnection(database, username, password, owner);
    if (con >= 0) {
      int ret = fwConfigurationDB_adoptProjConfig(computerName,project);
    }
    else DebugTN("No Connection"+con);
  }
  else DebugTN("Connection parameters empty");
  
   int cl =  fwInstallationDB_closeDBConnection(); 
}

int fwConfigurationDB_adoptProjConfig(string computerName, string project, bool adoptComponents = true, bool adoptPaths = true, bool adoptSysConnect = true)
{
  int result = 0;
  if (adoptComponents)
  {
    result = fwConfigurationDB_adoptInstalledComponents(computerName, project);
  }
  if (result == 0 && adoptPaths)
  {
    result = fwConfigurationDBSystemInformation_adoptProjectPaths(computerName, project);
  }
  if (result == 0 && adoptSysConnect)
  {
    result = fwConfigurationDB_adoptSystemConnectivity(computerName, project);
  }
  return result;
}

int fwConfigurationDB_adoptInstalledComponents(string computerName, string project, string groupName = "")
{
  dyn_float df;
  dyn_string ds;
  string group;
  int error = 0;  
//  string tagId = $id;

  int groupId = -1;
  bool registerGroup = false;
  bool force = false;
  
   if (strlen(groupName)==0)
    group = "g"+project;
  else  
    group = groupName;
    
  //Check if adoption is possible:
  if(!force && !fwConfigurationDBSystemInformation_isAdoptionPossible(computerName, project))
  {
    //fwInstallation_throw("Project configuration cannot be adopted as there are conflicts at the component level. These conflicts must be resolved first!");
    return -1;
  }
  
  //Invalidate all previous project groups if told to force adoption:
  if(force)
    fwInstallationDBSystemInformation_removeAllProjectGroups(project, computerName);
  
  //check if already registered:
  fwInstallationDB_isGroupRegistered(group, groupId);
    
  if(groupId < 0)
    registerGroup = true;
  else 
  {
    if(fwInstallationDBSystemInformation_unregisterAllGroupComponents(group) != 0)
    {
      DebugTN("Failed to unregister previous components in default group.");
      return -1;
    }
    registerGroup = false;
  } 
  
  if(registerGroup)
  {
    if(fwInstallationDB_registerGroup(group) != 0)
    {
      DebugTN("Could not create new group in DB.\nGroup names must be unique.");
      return -1;
    }
  }
  
  //Read components from table and and register them in the group
  //if they are not currently already assigned through another group
  
  dyn_dyn_mixed instComponents, reqComponents;
  fwInstallationDB_getCurrentProjectComponents(instComponents, project, computerName);
  fwInstallationDB_getProjectComponents(reqComponents, project, computerName);

  //DebugN(instComponents, "inst**************req", reqComponents);  
  for(int i = 1; i <= dynlen(instComponents); i++)
  {
    bool found = false;
    for(int j = 1; j <= dynlen(reqComponents); j++)
    {
      if(instComponents[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] == reqComponents[j][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] &&
         instComponents[i][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX] == reqComponents[j][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX])
      {
        DebugN("instComponents[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX]", instComponents[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX], instComponents[i][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX]);
        found = true;
        break;
      }
    } 
    //DebugN("Registering " + componentName + " v." + componentVersion);
    if(!found)
    {
      if(fwInstallationDB_registerComponentInGroup(instComponents[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX], 
                                                   instComponents[i][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX], 
                                                   instComponents[i][FW_INSTALLATION_DB_COMPONENT_SUBCOMP_IDX], 
                                                   instComponents[i][FW_INSTALLATION_DB_COMPONENT_DESC_FILE_IDX], 
                                                   group) != 0)
      {
        ++error;
        DebugN("ERROR: Could not add component: " + instComponents[i][FW_INSTALLATION_DB_COMPONENT_NAME_IDX] + " v." + instComponents[i][FW_INSTALLATION_DB_COMPONENT_VERSION_IDX] + " into group: " + group); 
      }
    }
  }
  
  //DebugN("Adding group to project: " + group);
  if(fwInstallationDB_registerGroupInProject(group, 1, 1, 1, 0, "", project, computerName) != 0)
  {
    DebugTN("$1:Cannot adopt project configuration.\nCould not add group: " + group + " to project: " + project + " in computer: " + computerName); 
    return -1;
  }
  return 0; 
}

int fwConfigurationDB_adoptSystemConnectivity(string computerName, string project)
{
  dyn_mixed projectProperties;
  int projectId;
  if (fwInstallationDB_getProjectProperties(project, computerName, projectProperties, projectId) != 0)
  {
    DebugTN("Failed to get project properties for project " + project + " on computer " + computerName);
    return -1;
  }
  DebugTN(project,computerName,projectId,projectProperties);
  //get all current connections
  dyn_mixed connectedSystemsInfo;
  if (fwInstallationDB_getSystemConnectivity(projectProperties[FW_INSTALLATION_DB_PROJECT_SYSTEM_NAME], computerName, connectedSystemsInfo, true) != 0)
  {
    DebugTN("Failed to get connected systems information for system " + projectProperties[FW_INSTALLATION_DB_PROJECT_SYSTEM_NAME]);
    return -1;
  }
  
  // delete all required connections
  if (fwInstallationDB_deleteAllRequiredSystemConnections(projectProperties[FW_INSTALLATION_DB_PROJECT_SYSTEM_NAME]) != 0)
  {
    DebugTN("Failed to delete required connections for project " + project + " on computer " + computerName);
    return -1;
  }
  
  // insert all current as required
  for (int i = 1; i<=dynlen(connectedSystemsInfo); i++)
  {
    if (fwInstallationDB_addSystemRequiredConnection(projectProperties[FW_INSTALLATION_DB_PROJECT_SYSTEM_NAME], connectedSystemsInfo[i][FW_INSTALLATION_DB_SYSTEM_NAME]) != 0)
    {
      DebugTN("Failed to register connection between " + projectProperties[FW_INSTALLATION_DB_PROJECT_SYSTEM_NAME] + " and " + connectedSystemsInfo[i][FW_INSTALLATION_DB_SYSTEM_NAME]);
      return -1;
    }
  }
  
  return 0;
}

