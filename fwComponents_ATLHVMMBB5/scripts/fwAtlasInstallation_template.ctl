main()
{
  dyn_string dps = dpNames("fwInstallation_*", "_FwInstallationComponents");
  string sysName = getSystemName();
  strreplace(sysName, ":", "");
  for (int i=1;i<=dynlen(dps);++i) {
    dpSetWait(dps[i]+".installationDirectory", "/localdisk/winccoa/fwComponents_"+sysName);
  }
  dpSetWait("fwInstallationInfo.installationDirectoryPath", "/localdisk/winccoa/fwComponents_"+sysName);

  dpSetWait("_fwAccessControlSetup.AdministrationPrivileges.Domains", sysName+":AuthControl",
            "_fwAccessControlSetup.AdministrationPrivileges.Groups", sysName+":AuthControl",
            "_fwAccessControlSetup.AdministrationPrivileges.Users", sysName+":AuthControl");

  // delete dps created by unDistControl in template
  dyn_string dps = dpNames("_unDistributedControl_ATLTEMPLAT*", "_UnDistributedControl");
  for (int i=1;i<=dynlen(dps);++i) {
    DebugTN("fwAtlasInstallation_template.ctl: deleting "+dps[i]);
    dpDelete(dps[i]);
  }
  //  dpDelete("_unDistributedControl_ATLTEMPLATE_315");

//  DebugTN("Set up DPs. Now setting FSM DNS node...");

//  string dnsNode;
//  string machineName = _WIN32 ? getenv("COMPUTERNAME") : getenv("HOSTNAME");
//  if (strpos(getHostByName(machineName), "10.")<0) dnsNode = "pcaticstest01";
//  else dnsNode = "pcatlfsmdns1,pcatlfsmdns2";
//  dpSet("ToDo.dim_dns_node", dnsNode, "ToDo.autoStart", 0);

  DebugTN("Fixing description and activating archiving alerts.");

  //
  // setting some default descriptions
  //
  string subdet = fwAtlas_getSubdetectorId(sysName);
  dpSetDescription("_RDBArchive.dbConnection.connected" , subdet+" RdbArchiving "+sysName+" Connection");
  dpSetDescription("_RDBArchive.buffer.currentBlocks" , subdet+" RdbArchiving "+sysName+" NumberOfBuffers");
  dpSetDescription("fwAtlas_UIMon.NumOpenUIs" , subdet+" WccOaSystem "+sysName+" NumberOfUserInterfaces");

  // activate alert for RDB connection
  //
  dyn_string ex;
  fwAlertConfig_activate("_RDBArchive.dbConnection.connected", ex);
  DebugTN("Finished fwAtlasInstallation_template.ctl.");
  return;
}
