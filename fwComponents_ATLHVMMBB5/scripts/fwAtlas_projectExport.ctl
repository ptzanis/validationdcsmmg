//
// Script for automatic project export
//
// Author: Stefan Schlenker, CERN
//

#uses "fwAtlas.ctl"
#uses "fwInstallation.ctl"

//
// export DPs or DPTs according to ASCII manager config preset
//
bool fwAtlas_exportFromAsciiConfig(string configDP)
{
	string fileName;
	bool dpt, dp, alias, original, param;
	dyn_string dps, dpts;
	dpGet(configDP + ".ASCII_Datei:_original.._value", fileName,
				configDP + ".OutFilter.DPTypen:_original.._value", dpt,
				configDP + ".OutFilter.DP:_original.._value", dp,
				configDP + ".OutFilter.Alias:_original.._value", alias,
				configDP + ".OutFilter.Originalwerte:_original.._value", original,
				configDP + ".OutFilter.Parametrierung:_original.._value", param,
 				configDP + ".DP:_original.._value", dps,
				configDP + ".DPTypen:_original.._value", dpts);
	string filter;
	if (dpt) filter += "T";
	if (dp) filter += "D";
	if (alias) filter += "A";
	if (original) filter += "O";
	if (param) filter += "P";
	if (dynlen(dps)==1 && dps[1]=="alle") dps = makeDynString();
	if (dynlen(dpts)==1 && dpts[1]=="alle") dpts = makeDynString();
 	fwInstallationPackager_createDpl(fileName, dps, dpts, filter);
	if (isfile(fileName) && getFileSize(fileName) > 100
			&& (getCurrentTime() - getFileModificationTime(fileName))<10 ) {
		information("DP export: successfully created "+fileName);
		return true;
	}
	else {
		error("DP export: error creating "+fileName);
		return false;
	}
}

//
// increase patch number and update date
// within component XML file
//
bool fwAtlas_updateComponentXml(string componentName,  string path)
{
	string xmlFile = path + componentName + ".xml";
  dyn_dyn_mixed componentInfo;
// 	fwInstallationXml_load(xmlFile, componentName, componentVersion, componentDate,
// 		 requiredComponents, isSubComponent, initScripts, deleteScripts,
// 		 postInstallScripts, postDeleteScripts, configFiles, asciiFiles,
// 		 panelFiles, scriptFiles, libraryFiles, otherFiles, xmlDesc, dontRestart, helpFile,
//     subComponents, config_windows, config_linux, scriptsToBeAddedFiles, comments);
	fwInstallationXml_load(xmlFile, componentInfo);
	DebugTN("ComponentInfo:", componentInfo);

	int major, minor, patch;
  string version = componentInfo[FW_INSTALLATION_XML_COMPONENT_VERSION];
	sscanf(version, "%d.%d.%d", major, minor, patch);
	sprintf(version, "%d.%d.%d", major, minor, patch+1);
  componentInfo[FW_INSTALLATION_XML_COMPONENT_VERSION] = version;
  componentInfo[FW_INSTALLATION_XML_COMPONENT_DATE] = formatTime("%Y/%m/%d", getCurrentTime());
  
  dyn_string xmlDesc;
//   fwInstallationXml_create(
//     path, componentInfo[FW_INSTALLATION_XML_COMPONENT_NAME], version, componentInfo[FW_INSTALLATION_XML_COMPONENT_DATE],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_REQUIRED_COMPONENTS], componentInfo[FW_INSTALLATION_XML_COMPONENT_IS_SUBCOMPONENT],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_INIT_SCRIPTS], componentInfo[FW_INSTALLATION_XML_COMPONENT_DELETE_SCRIPTS],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_POST_INSTALL_SCRIPTS], componentInfo[FW_INSTALLATION_XML_COMPONENT_POST_DELETE_SCRIPTS],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_CONFIG_FILES], componentInfo[FW_INSTALLATION_XML_COMPONENT_DPLIST_FILES],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_PANEL_FILES], componentInfo[FW_INSTALLATION_XML_COMPONENT_SCRIPT_FILES],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_LIBRARY_FILES], componentInfo[FW_INSTALLATION_XML_COMPONENT_OTHER_FILES],
//     componentInfo[FW_INSTALLATION_XML_COMPONENT_DONT_RESTART], xmlDesc);
  fwInstallationXml_createComponentFile(path, componentInfo);

  if (isfile(xmlFile) && getFileSize(xmlFile) > 100
    && (getCurrentTime() - getFileModificationTime(xmlFile))<10 ) {
    information("XML file "+xmlFile+" successfully recreated");
    return true;
  }
  else {
  		error("Error modifying XML file "+xmlFile);
  		return false;
  }
}


main()
{
	string componentName = getSystemName();
	strreplace(componentName, ":", "");

	// find Ascii configs mactching <systemName>_* and export
	//
	dyn_string configDps = dpNames("*", "_AsciiConfig");
  dyn_string validConfDps;
	DebugTN("existing ascii configs: "+configDps);
	for (int i=1;i<=dynlen(configDps);++i) {
		string name;
		string dp = configDps[i];
		dpGet(dp+".Name", name);
		if (strpos(name, componentName+"_")>-1) {
      information("Exporting ascii config "+name+" ...");
      fwAtlas_exportFromAsciiConfig(dp);
    }
	}

	string path = _WIN32 ? REPOSITORY_WIN : getenv("DCS_REPO");
	path += "/ATLAS_DCS_" + substr(getSystemName(), 3, 3)	+ "/" + componentName + "/";

	//
	// FSM export
	//
	dyn_string doNotExportTypes = makeDynString
		("fwOT_ATLAS_STATUS", "fwOT_ATLAS_DU_STATUS", "fwOT_ATLAS_CU",
		 "fwOT_ATLAS_CU_DEVICE", "fwOT_ATLAS_CU_PASSIVE");

	// 1st FSM types
	//
	if ( fwInstallationPackager_exportFsm
			 (componentName+"_FSM", path, makeDynString("Types only"), doNotExportTypes)==0
			 ) information("FSM types export: successfully finished to "+path);
	else error("FSM types export: error while exporting to "+path);

	// 2nd FSM trees
	//
	dyn_string domains, ex;
	fwTree_getChildren("FSM", domains, ex);
	DebugTN("Exporting domains "+domains+" to "+path);
	if ( fwInstallationPackager_exportFsm
			 (componentName+"_FSM", path, domains, doNotExportTypes)==0
			 ) information("FSM export of "+domains+": successfully finished to "+path);
	else error("FSM export: error while exporting "+domains+" to "+path);

	// copy config files
	//
	string configFile = getPath(CONFIG_REL_PATH, "config");
	string progsFile = getPath(CONFIG_REL_PATH, "progs");
	if ( copyFile(configFile, path+"config") && copyFile(progsFile, path+"config")
			 ) information("Config files successfully copied to "+path);
	else error("Error while copying config files to "+path);

	// update XML file
	//
  DebugTN("path:", path);
	fwAtlas_updateComponentXml(componentName, path);
	fwAtlas_updateComponentXml(componentName+"_FSM", path);
}
