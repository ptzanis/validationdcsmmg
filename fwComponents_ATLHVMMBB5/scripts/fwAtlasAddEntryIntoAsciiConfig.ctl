main()
{
  dyn_string names = dpNames("*","_AsciiConfig");
  for (int i = 1; i < dynlen(names); i++)
  {
    dyn_string  CNSPaths,CNSViews;
    dpGet(names[i]+".CNSPaths",CNSPaths,names[i]+".CNSViews",CNSViews);
    if (dynlen(CNSPaths) == 0) 
    {
      dynAppend(CNSPaths,"");
      dpSet(names[i]+".CNSPaths",CNSPaths);
    }
    if (dynlen(CNSViews) == 0) 
    {
      dynAppend(CNSViews,"");
      dpSet(names[i]+".CNSViews",CNSViews);
    }
  }
}
