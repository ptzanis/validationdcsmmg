#uses "fwAtlas.ctl"
#uses "fwFsmUi.ctl"
#uses "fwConfigs/fwArchive.ctl"

//
// global variables for FSM widget callbacks
//
string gStatus;
string gStatusColor;
bool gShortenStatus;
string gState;
string gStateColor;
bool gShortenState;
int gShortNumChars;
string gObject;
string gDomain;
string gTextWidget;
string gColorWidget;
string gCurrentMode;
int gMenuValidState=0;
int gMenuValidAccess=0;
int gMenuValidMode=0;
int gActionMenuACCallbackInitiated=0;          // 9/2010 SZ

const int STATUSNONINITIALIZED = 0;
const int STATUSUNINITIALIZED = 1;
const int STATUSINITIALIZED = 2;

/******************************************************************/
/******************  STATUS related functions *********************/
/******************************************************************/

//
// Get the current STATUS of a FSM unit
//
fwFsmAtlas_getStatus(string domain, string object, string& status)
{
  fwCU_getState(domain+"::STATUS_"+object, status);
}

//
// Set the STATUS for a FSM unit
// works only on the local system!!!
//
fwFsmAtlas_setStatus(string domain, string device, string status, string message = "", bool waitForCompletion = true)
{
  string statusNode = domain+fwFsm_separator+"STATUS_"+device;
  if (dpExists(statusNode)) {
    if (status=="ALARM") status = "ERROR"; // backward compatibility
    string node = domain+"::STATUS_"+device;
    //     startThread("_fwFsmAtlas_setStatus", node, status,
    // 		domain+"::"+device+": "+message);
    _fwFsmAtlas_setStatus(node, status,	domain+"::"+device+": "+message, waitForCompletion);
  }
}

//
// For internal use only,
// If the status object is not alive, wait up to 8 minutes
// to become alive before switching the status
//
synchronized _fwFsmAtlas_checkInitMapping()
{
//  DebugTN("_fwFsmAtlas_checkInitMapping()");
  if (!globalExists("_fwFsmAtlas_statusInitialized")) {
    // if this mapping is created, it means that setStatus was called before all objects were alive
//    DebugTN("_fwFsmAtlas_checkInitMapping(): adding status mapping.");
    addGlobal("_fwFsmAtlas_statusInitialized", MAPPING_VAR);
  }
}

void _fwFsmAtlas_statusCallback(string dpe, string status)
{
//  DebugTN("_fwFsmAtlas_statusCallback("+dpSubStr(dpe, DPSUB_SYS_DP_EL)+", "+status+")");
  if (status=="DEAD") return;
  if (status=="UNINITIALIZED") _fwFsmAtlas_statusInitialized[dpSubStr(dpe, DPSUB_SYS_DP_EL)] = STATUSUNINITIALIZED;
  else _fwFsmAtlas_statusInitialized[dpSubStr(dpe, DPSUB_SYS_DP_EL)] = STATUSINITIALIZED;
}

_fwFsmAtlas_setStatus(string node, string status, string message, bool waitForCompletion)
{
//  DebugTN("_fwFsmAtlas_setStatus("+node+") called with status = "+status);
  string currentStatus;
  string statusDp = getSystemName()+node+".fsm.currentState";
  strreplace(statusDp, "::", "|");
  dpGet(statusDp, currentStatus);
  if (currentStatus == status) return; // do nothing if Status does not change
  
  _fwFsmAtlas_checkInitMapping();

  if (currentStatus == "DEAD") {
//    DebugTN("_fwFsmAtlas_setStatus("+node+") is still DEAD.");

    // wait for STATUS to change to UNITIALIZED
//    DebugTN("_fwFsmAtlas_setStatus("+node+") waiting to come up.");
    _fwFsmAtlas_statusInitialized[statusDp] = STATUSNONINITIALIZED;
    dpConnect("_fwFsmAtlas_statusCallback", statusDp);
    int timeout = 480*2; // 8mins
    while (_fwFsmAtlas_statusInitialized[statusDp]==STATUSNONINITIALIZED && timeout > 0) {
      delay(0, 500);
      --timeout;
    }
    if (!timeout) error("_fwFsmAtlas_setStatus("+node+") timed out, status object still DEAD. This must be followed up!!");
    dpDisconnect("_fwFsmAtlas_statusCallback", statusDp);
//    DebugTN("_fwFsmAtlas_setStatus("+node+") disconnected, last error:"+getLastError());
  }
  else _fwFsmAtlas_statusInitialized[statusDp] = STATUSUNINITIALIZED;

  //
  // switch STATUS object state
  //
//  DebugTN("_fwFsmAtlas_setStatus("+node+") sending command, currentStatus: "+currentStatus);

  if (waitForCompletion) dpConnect("_fwFsmAtlas_statusCallback", false, statusDp);

  // send smi command to status object
  //
  int ret = fwCU_sendCommand(node, "GOTO_"+status);
  if (ret!=1) error("_fwFsmAtlas_setStatus("+node+") failed setting status command DPE");
//  DebugTN("_fwFsmAtlas_setStatus("+node+") sendCommand returned: "+ret);
  
  // wait for status change on users request
  //
  if (waitForCompletion) {
    int timeout = 100; // max 20s
    while (_fwFsmAtlas_statusInitialized[statusDp]!=STATUSINITIALIZED && timeout > 0) {
      delay(0, 200);
      --timeout;
    }
    if (!timeout) error("_fwFsmAtlas_setStatus("+node+") timed out, status command failed. This must be followed up!!");
    dpDisconnect("_fwFsmAtlas_statusCallback", statusDp);
  }

  // raise message
  //
  message += "STATUS change from "+currentStatus+" to "+status;
  if (currentStatus!="DEAD" && currentStatus!="UNINITIALIZED") { // no prints during startup
    switch(status) {
    case "OK":
      information(message, node);
      break;
    case "WARNING":
      warning(message, node);
      break;
    case "ERROR":
      error(message, node);
      break;
    case "FATAL":
      fatal(message, node);
      break;
    }
  }
}

fwFsmAtlas_connectStatus(string callback, string object)
{
  fwCU_connectState(callback, "STATUS_"+object);
}

fwFsmAtlas_connectDUStatus(string callback, string domain, string object)
{
  fwDU_connectState(callback, true, domain, "STATUS_"+object);
}

bool fwFsmAtlas_isBusy(string domain, string obj)
{
  string dp;
  obj = fwFsm_convertAssociated(obj);
  obj = fwFsm_convertAssociated(obj);
  fwUi_getSysPrefix(domain, dp);
  dp += fwFsm_separator+obj+".fsm";
  if(dpExists(dp)) {
    string command;
    dpGet(dp+".executingAction:_online.._value", command);
    if (command=="") return false;
  }
  return true;
}

fwFsmAtlas_connectStatusButton(string domain, string object, bool shortStatus = false, string textWidget = "", string colorWidget = "")
{
  gShortenStatus = shortStatus;
  gTextWidget = textWidget;
  gColorWidget = colorWidget;
  gCurrentMode = 0;
  fwUi_connectCurrentState("_fwFsmAtlas_show_status_text", domain, object);
  string obj = object;
  strreplace(obj, "STATUS_", "");
  fwFsmUi_connectModeBits("_fwFsmAtlas_show_status_mode", domain, obj,1,"");
}

_fwFsmAtlas_show_status_text(string dp, string status)
{
  if (status == "") return;
  gStatus = status;
  if (gTextWidget!="none") setValue("", "text", gShortenStatus ? _fwFsmAtlas_shorten_status(status) : status);
  gStatusColor = fwFsmAtlas_getStatusColor(status);
  if (gColorWidget!="none") _fwFsmAtlas_show_status_mode("", gCurrentMode);
}

_fwFsmAtlas_show_status_mode(string modeDpe, bit32 bits)
{
  // use mode bits of associated domain object to show status mode
  //
  //  bit32 bits2 = fwFsmUi_getOwnModeBits();
  //  if (bits!=bits2) DebugTN("different modes for "+modeDpe+"!!!!!!!", bits, bits2);
  gCurrentMode = bits;
  //   DebugTN(modeDpe+" usestate: "+getBit(bits, FwUseStatesBit)+" sendcommands: "+getBit(bits, FwSendCommandsBit)+" free: "+getBit(bits, FwFreeBit)+" CUfree: "+getBit(bits, FwCUFreeBit));
  if (getBit(bits, FwUseStatesBit) && !getBit(bits, FwFreeBit)) setValue("", "backCol", gStatusColor);
  else setValue("", "backCol", "_3DFace");
}

// replaces normal status string by short version
// should be connected in initialize() of a status widget
//
fwFsmAtlas_show_short_status(string dp, string state)
{
  _fwFsmUi_show_state(dp, state);
  State = fwUi_convertLocalCurrentState($domain, $obj, state);
  if (StateWidget != "none") {
    setValue(StateWidget, "text", _fwFsmAtlas_shorten_status(State));
  }
}

string _fwFsmAtlas_shorten_status(string state)
{
  string stateLabel = state;
  if (state=="WARNING") stateLabel = "W";
  else if (state=="ALARM") stateLabel = "E";
  else if (state=="ERROR") stateLabel = "E";
  else if (state=="FATAL") stateLabel = "F";
  else if (state=="DEAD") stateLabel = "D";
  else if (state=="UNINITIALIZED") stateLabel = "U";
  return stateLabel;
}

string fwFsmAtlas_getStatusColor(string status)
{
  string color = "";
  switch(status) {
  case "OK":
    color = "FwStateOKPhysics";
    break;
  case "WARNING":
    color = "FwStateAttention1";
    break;
  case "ERROR":
  case "ALARM":
    color = "FwStateAttention2";
    break;
  case "FATAL":
    color = "FwStateAttention3";
    break;
  case "UNINITIALIZED":
    color = "FwDead";
    break;
  case "DEAD":
    color = "FwDead";
    break;
  }
  return color;
}

//
// State connect function
//

fwFsmAtlas_connectStateButton(string domain, string object, bool shortState = false, string textWidget = "", string colorWidget = "", int shortNumChars = 1)
{
  gShortenState = shortState;
  gShortNumChars = shortNumChars;
  gTextWidget = textWidget;
  gColorWidget = colorWidget;
  gObject = object;
  gDomain = domain;
  gCurrentMode = 0;
  fwUi_connectCurrentState("_fwFsmAtlas_show_state_text", domain, object);
  fwUi_connectExecutingAction("_fwFsmAtlas_show_busy", domain, object);
  if (colorWidget!="none") fwFsmUi_connectModeBits("_fwFsmAtlas_show_state_mode", domain, object, 1,"");
}

_fwFsmAtlas_show_state_text(string dp, string state)
{
//  DebugTN("_fwFsmAtlas_show_state_text("+dp+", "+state+")");
  if (state == "") return;
  gState = state;
  string shortState = substr(state, 0, gShortNumChars);
  if (gTextWidget!="none") setValue("", "text", gShortenState ? shortState : state);
  fwUi_getLocalObjStateColor(gDomain, gObject, state, gStateColor);
  if (gColorWidget!="none") _fwFsmAtlas_show_state_mode(dp, gCurrentMode);
}

_fwFsmAtlas_show_state_mode(string modeDpe, bit32 bits)
{
  DebugFTN("fwFsmAtlas_UI_DEBUG","_fwFsmAtlas_show_state_mode("+modeDpe+", "+bits+"), state="+gState+","+gStateColor);
  // use mode bits of associated domain object to show status mode
  //
  gCurrentMode = bits;
  if( (getBit(bits, FwUseStatesBit))
      && (!getBit(bits, FwSendCommandsBit))
      && (!getBit(bits, FwCUFreeBit))) {
    DebugFTN("fwFsmAtlas_UI_DEBUG", "_fwFsmAtlas_show_state_mode("+modeDpe+", setting bg color "+gStateColor);
    /*if (enabled==2)*/ 
    setValue(gColorWidget,"backCol", gStateColor);
    if (gTextWidget!="none") {
      if (FSMBusy) setValue(gColorWidget,"foreCol","grey");
      else {
 if (gStateColor=="FwStateShutdown") setValue(gColorWidget,"foreCol", "FwAtlasFg");
	else setValue(gColorWidget,"foreCol", "_3DText");
      }
    }
    // 		else setValue(gColorWidget,"backCol", gStateColor+"NotOwner");
  }
  else if ( (getBit(bits, FwFreeBit)) || (!getBit(bits, FwUseStatesBit)) ) {
    setValue(gColorWidget,"backCol", "_3DFace");
    if (gTextWidget!="none") {
      if (FSMBusy) setValue(gColorWidget,"foreCol","grey");
      else setValue(gColorWidget,"foreCol", "_3DText");
    }
  }
  else {
    /* if (enabled==2) */
    setValue(gColorWidget,"backCol", gStateColor);
    if (gTextWidget!="none") {
      if (FSMBusy) setValue(gColorWidget,"foreCol","grey");
      else {
	if (gStateColor=="FwStateShutdown") setValue(gColorWidget,"foreCol", "FwAtlasFg");
	else setValue(gColorWidget,"foreCol", "_3DText");
      }
    }
    // 		else setValue(gColorWidget,"backCol", gStateColor+"NotOwner");
  }

  //   if (getBit(bits, FwUseStatesBit) && !getBit(bits, FwFreeBit)) setValue("", "backCol", gStateColor);
  //   else setValue("", "backCol", "_3DFace");
}

_fwFsmAtlas_show_busy(string dp, string action)
{
  DebugFTN("fwFsmAtlas_UI_DEBUG", "action callback", dp, action);
  string color = (gStateColor=="FwStateShutdown") ? "FwAtlasFg" : "_3DText";
//  getValue(StateWidget,"foreCol",color);

  if(gTextWidget == "none") return;
  //   if(action == "") setValue(gTextWidget, "enabled", 1);
  //   else setValue(gTextWidget, "enabled", 0);
  if(action == "") {
    FSMBusy = 0;
    setValue(StateWidget,"foreCol",color);
    //		setValue(StateWidget,"enabled",1);
  }
  else {
    FSMBusy = 1;
    setValue(StateWidget,"foreCol","grey");
    //		setValue(StateWidget,"enabled",0);
  }
}

bool fwFsmAtlas_areYouSure(string question = "Are you sure")
{
  dyn_string menu = makeDynString
    ("PUSH_BUTTON, "+question+"?, 1, 0",
     "SEPARATOR",
     "PUSH_BUTTON, YES, 2, 1",
     "PUSH_BUTTON, NO, 3, 1");

  int answer = 0;
  popupMenu(menu, answer);
  return (answer==2);
}

//
// opens a context menu at the widget position with all
// possible commands depending on the current state
//
fwFsmAtlas_openActionsMenu(string domain, string object)
{
  //
  // use global variable to inhibit sending of action if
  //   AC privs or user changed
  //   object state changed
  dyn_string ex;
  gMenuValidState = -1;
  gMenuValidAccess = -1;
  gMenuValidMode = -1;
  if (!gActionMenuACCallbackInitiated) fwAccessControl_setupPanel("_fwFsmAtlas_actionMenuAcCallback", ex);
  else gMenuValidAccess = 0;
  fwUi_connectCurrentState("_fwFsmAtlas_actionMenuStateChanged", domain, object);
  fwUi_connectModeBits("_fwFsmAtlas_actionMenuMode",domain,object);
  int timeout = 600;
  while ( (gMenuValidState == -1 || gMenuValidAccess == -1 || gMenuValidMode == -1)
	  && timeout) {
    delay(0,100);
    --timeout;
  }
  gMenuValidState = 1;
  gMenuValidAccess = 1;
  gMenuValidMode = 1;
  // get actions and action types
  //
  dyn_string actions;
  dyn_int visis;
  string state;
  fwDU_getState(domain, object, state);
  //   DebugTN(fwFsmAtlas_isBusy(domain, object), FSMBusy);
  if (!fwFsmAtlas_isBusy(domain, object)) {
    fwUi_getLocalObjStateActions(domain, object, state, actions, visis);
  }
  else {
    dynAppend(actions, "AbortCommand");
    dynAppend(visis, 2);
    dynAppend(actions, "RestartFSM");
    dynAppend(visis, 2);
  }
  bool force_operate = false;
  if((!dynlen(actions)) && (State == "DEAD")) {
    dynAppend(actions, "RestartFSM");
    dynAppend(visis, 2);
    force_operate = true;
  }
  //  DebugTN("actions",domain, object, state, actions, visis);
  if (dynlen(actions)==0) {
    if (State == "DEAD") {
      dynAppend(actions, "RestartFSM");
      dynAppend(visis, 2);
    }
    else {    // no action. release callbacks 9/2010 SZ
      fwUi_disconnectModeBits("_fwFsmAtlas_actionMenuMode",domain,object);
      fwUi_disconnectCurrentState("_fwFsmAtlas_actionMenuStateChanged", domain, object);
      return; 
    }
  }
  DebugFTN("fwFsmAtlas_UI_DEBUG", "Actions:", actions);

  // check access privileges
  //
  int operate_domain, operate_object;
  fwUi_getAccess(domain);
  fwUi_getDomainOperation(fwUi_getTopDomain(domain, object), operate_domain);
  fwUi_getOperation(domain, object, operate_object);
  int operateable = (operate_domain && operate_object);
  int accessControlOperator = fwFsmUi_getOperatorAccess(domain, object);
  int accessControlExpert = fwFsmUi_getExpertAccess(domain, object);
  bit32 mode = fwUi_getModeBits(domain, object);
  int operate = ( ( getBit(mode, FwOwnerBit)
		    || ( (!getBit(mode, FwExclusiveBit))
			 && (!getBit(mode, FwFreeBit)) ) )
		  && ( getBit(mode, FwUseStatesBit)
		       || (getBit(mode, FwSendCommandsBit)) )
		  && operateable
		  && accessControlOperator);
  DebugFTN("fwFsmAtlas_UI_DEBUG", "access",domain, object, operate_domain, operate_object, operateable,
	  accessControlOperator, accessControlExpert, operate, mode);

  // fill menu with one button per action,
  // disable actions if non-privileged user
  //
  dyn_string menu;
  //   if (AccessControlExpert) dynAppend(menu, "PUSH_BUTTON, Expert actions:, 1, 0");
  for (int i = 1; i <= dynlen(actions) ; ++i) {
    int actionAvailable = operate;
    string text = actions[i];
    if (visis[i] == 2) {
      if (!accessControlExpert) actionAvailable = 0;
      text = "EXPERT COMMAND: "+text;
    }
    if (force_operate) actionAvailable = 1;
    string menuItem = "PUSH_BUTTON, " + text + ", " + i + ", " + actionAvailable;
    dynAppend(menu, menuItem);
  }
  //  DebugTN("Actions Menu: ", menu);

  int answer = 0;
  popupMenu(menu, answer);
  //  DebugTN("Menu answer = "+answer);

  if (answer == 0) {
    DebugTN("No action selected, abort");
    fwUi_disconnectCurrentState("_fwFsmAtlas_actionMenuStateChanged", domain, object);
    return;
  }
    
  string command = actions[answer];
  strreplace(command, "EXPERT COMMAND: ", "");
  
  // require command confirmation for logical objects,
  // check also LHC state
  //
  string message = " Send "+command;
  if (command=="GOTO_READY") {
    string fsmType, systemName;
    fwCU_getType(domain + "::" + object, fsmType);
    dyn_string states;
    fwUi_getDomainSys(domain, systemName);
    DebugTN("Looking for states of "+systemName+fsmType);
    fwFsm_getObjectStates(systemName+fsmType, states);
    DebugN("FSM object type is " + fsmType + ", states: ", states);
    if (dynContains(states, "STANDBY")) { 
      bool lhcConnected;
      unDistributedControl_isConnected(lhcConnected, "ATLGCSLHC:");
      if (lhcConnected) {
        string beamMode;
        dpGet("ATLGCSLHC:lhcStatus.beamMode.value", beamMode);
        if (beamMode!="STABLE BEAMS" && beamMode!="NO BEAM") {
          message = "WARNING! Beam conditions not stable! Send "+command+" anyway?";
        }
        else DebugTN("Beam stable or No Beam.");
      }
      else DebugTN("Could not check beam mode, LHC system not connected.");
    }
  }
  if (!fwFsm_isDU(domain, object)
    && !fwFsmAtlas_areYouSure(message)) {
    fwUi_disconnectCurrentState("_fwFsmAtlas_actionMenuStateChanged", domain, object);
    fwUi_disconnectModeBits("_fwFsmAtlas_actionMenuMode",domain,object);
    return;
  }

  
  // send the action in case no other user logged in
  // or the state changed meanwhile
  //
  if (answer>0) {
    if (gMenuValidAccess!=1 || gMenuValidState!=1 || gMenuValidMode!=1) {
      langString msgText="The command is not anymore valid,";
      if (gMenuValidState!=1) msgText+=" the State changed!";
      else if (gMenuValidAccess!=1) msgText+=" the User changed!";
      else if (gMenuValidMode!=1) msgText+=" the Mode changed!";
      msgText+="\nEnsure that there is no conflicting action going on and try again.";
      popupMessage("_Ui_1", msgText);
      error(msgText);
      fwUi_disconnectCurrentState("_fwFsmAtlas_actionMenuStateChanged", domain, object);
      fwUi_disconnectModeBits("_fwFsmAtlas_actionMenuMode",domain,object);
      return;
    }
    if (command == "RestartFSM") {
      action("Restarting "+domain+"::"+object);
      fwFsmUi_restartFSM(domain, object);
    }
    else if (command == "AbortCommand") {
      action("Aborting command for "+domain+"::"+object);
      fwFsmUi_abort(domain, object);
    }
    else {
      string type;
      fwFsm_getObjectType(domain+"::"+object, type);
      string sys;
      fwUi_getDomainSys(domain, sys);
      dyn_string parameters;
      fwFsm_readObjectActionParameters(sys+type, state, command, parameters);
      if (dynlen(parameters)) {
	//         getValue(actionsWidget, "position", x,y);
	bool askUser = false;
	for(int i = 1; i <= dynlen(parameters); ++i) {
	  dyn_string items = strsplit(parameters[i], " ");
	  string parameter = items[2];
          string val_type = items[1];
          string value = "";
          if (dynlen(items) > 2) {		
            value = items[4];
            if (strpos(value, "\"")==0) {
              value = substr(value, 1, strlen(value)-2);
            }
          }
          if (strreplace(value, "$dp=", "") == 0) {
            askUser = true;
          }
          else {
            strreplace(value,"$domain", domain);
            strreplace(value,"$obj", object);
            if(dpExists(value)) dpGet(value, value);
            fwFsmUi_addParameter(command, parameter, val_type, value);
          }
        }
        if (askUser) {
	  int x, y;
	  getValue("", "position", x, y);
	  ChildPanelOn("fwFSM/ui/fwFsmParams.pnl", "Params",
		       makeDynString(sys+type, state, command, domain, object), x+25, y);
	}
	else {
	  action("Sending command "+command+" to "+domain+"::"+object);
	  fwUi_sendCommand(domain, object, command);
	}
      }
      else {
	action("Sending command "+command+" to "+domain+"::"+object);
	fwUi_sendCommand(domain, object, command);
      }
    }
  }
  //   gMenuValidState=false;
  //   gMenuValidAccess=false;
  //   gMenuValidMode=false;
  
// release callbacks connected in this function. 9/2010 SZ

  fwUi_disconnectCurrentState("_fwFsmAtlas_actionMenuStateChanged", domain, object);  
  fwUi_disconnectModeBits("_fwFsmAtlas_actionMenuMode",domain,object);
  
}

_fwFsmAtlas_actionMenuStateChanged(string dp, string state)
{
  gMenuValidState = 0;
}

_fwFsmAtlas_actionMenuAcCallback(string s1, string s2)
{
  gActionMenuACCallbackInitiated = 1;
  gMenuValidAccess = 0;
}

_fwFsmAtlas_actionMenuMode(string s1, string s2)
{
  gMenuValidMode = 0;
}

//
// Set the State for a FSM unit and issue a log message
//
fwFsmAtlas_setDUState(string domain, string device, string state, string message = "")
{
  string currentState;
  fwUi_getCurrentState(domain, device, currentState);

  fwDU_setState(domain, device, state);

  if (currentState == state) return;
  if (state == "UNKNOWN") error("State change to UNKNOWN. "+message, domain+"::"+device);
  else information("State change to "+state+". "+message, domain+"::"+device);
}

//
// deprecated, use fwFsmAtlas_setArchiveConfig
//
bool fwFsmAtlas_setCUArchiveConfig(string domain)
{
  fwFsmAtlas_setArchive(domain);
}

//
// Activate archiving of FSM State, Status and Actions
//
bool fwFsmAtlas_setArchive(string domain, string object = "")
{
  if (object=="") object = domain;
  if (!dpExists(domain+"|"+object)) {
    error("fwFsmAtlas_setArchiveConfig: FSM DP for object "
	  +domain+"::"+object+" does not exist! No archive config set.");
    return false;
  }

  if (!dpExists(domain+"|STATUS_"+object)) {
    error("fwFsmAtlas_setArchiveConfig: FSM STATUS DP for domain "
	  +domain+" does not exist! No archive config set.");
    return false;
  }

  dyn_string dpes = makeDynString(domain+"|"+object+".fsm.currentState",
				  domain+"|"+object+".fsm.sendCommand",
				  domain+"|STATUS_"+object+".fsm.currentState");
  return fwFsmAtlas_setArchiveConfig(dpes,
				     DPATTR_ARCH_PROC_VALARCH,
				     DPATTR_COMPARE_OLD_NEW);
}

bool fwFsmAtlas_setArchiveConfig(dyn_string dpes,
				 int archiveType = DPATTR_ARCH_PROC_VALARCH,
				 int smoothProcedure = 0,
				 float deadband = 0.,
				 float timeInterval = 0.)
{
  // Set the archive configs
  information("Set archive config for "+dpes);
  fwArchive_setMultiple(dpes, FW_CONDDB_RDB_ARCHIVE_CLASS,
			archiveType, smoothProcedure, deadband,
			timeInterval, ex);
  return fwAtlas_checkException("Exception during setting archive config:",ex);
}



/******************************************************************/


// creates all button elements for a FSM node
//
dyn_string fwFsmAtlas_addFsmNodeButtons(string domain, string obj,
					int x, int y, bool isCU = true,
					string extension = "",
					dyn_string params = makeDynString())
{
  //  	DebugTN("addFsmNodeButtons "+domain+", "+obj);
  dyn_string panels;
  int next_x = x;
  domain = isCU ? obj : domain;
  dynAppend(panels, fwFsmAtlas_addObjButton
	    (domain, obj, next_x, y,
	     "objects/fsmAtlas/fwFsmTitle"+extension+".pnl",
	     makeDynString("$isMainPanel:1", "$domain:"+domain, "$obj:"+obj)));
  next_x = next_x+10;
  dynAppend(panels, fwFsmAtlas_addObjButton
	    (domain, obj, next_x, y,
	     "objects/fsmAtlas/fwSmiObj"+extension+".pnl", params));
  next_x = next_x-32;
  dynAppend(panels, fwFsmAtlas_addObjButton
	    (domain, "STATUS_"+obj, next_x, y,
	     "objects/fsmAtlas/fwSmiObjStatus"+extension+".pnl", params));
  next_x=next_x-57;
  if (isCU)	dynAppend(panels, fwFsmAtlas_addObjButton
			  ( gControlDomain, (gControlDomain == obj ? obj : obj+"::"+obj), next_x, y,
			    "objects/fsmAtlas/fwFsmLock"+extension+".pnl", params));
  else dynAppend(panels, fwFsmAtlas_addObjButton
		 (domain, obj, next_x, y,
		  "objects/fsmAtlas/fwFsmDevLock"+extension+".pnl", params));
  return panels;
}

//fwFsmAtlas_addObjButton:
/**  Place only a small FSM Object (only state and actions) in the FSM main panel.

@param node: The node name (CU) for this panel (available in $node).
@param obj: The object name (CU/LU/DU/Obj) (either $obj or the result of fwFsmUi_getXXXChildren()).
@param x: The X coordinate for the Object.
@param y: The Y coordinate for the Object.
@param panel: The filename of the panel to be placed, default: "fwFSM/ui/fwSmiObj.pnl".
@param params: Additional $parameters to be passed to the panel, default: none.
@return The name of the reference panel which was added
*/
string fwFsmAtlas_addObjButton(string domain, string obj, int& x, int y,
			       string panel = "fwFSM/ui/fwSmiObj.pnl",
			       dyn_string params = makeDynString())
{
  //  	DebugTN("Placing widget "+panel+" for "+domain+","+obj+" @",x,y);
  dynAppend(params, makeDynString("$domain:"+domain, "$obj:"+obj));
  addSymbol(myModuleName(), myPanelName(), panel,	obj+panel, params,
	    x, y, 0,1,1);
  x = x+130;
  return (obj+panel);
}

fwFsmAtlas_navigate(string domain, string obj, bool recordInHistory = true)
{
  // 	DebugTN("navigate "+domain+", "+obj);
  if (domain==gCurrentDomain && obj==gCurrentObject) {
    //     DebugTN("navigate: are already at "+domain+", "+obj);
    return;
  }
  if (recordInHistory) {
    _fwFsmAtlas_updateHistory
      (gCurrentDomain, gCurrentObject, gNaviBackHistory[1]);
    dynClear(gNaviFwdHistory[1]);
  }

  dpSetWait(gConfigDp+".domain", domain, gConfigDp+".object", obj);
  dpSet(gConfigDp+".closeFsmModule", true);
}

fwFsmAtlas_navigateSecondary(string domain, string obj, bool recordInHistory = true)
{
  // 	DebugTN("navigateSecondary "+domain+", "+obj);
  if (recordInHistory) {
    _fwFsmAtlas_updateHistory
      (gCurrentDomain, gCurrentObject, gNaviBackHistory[2]);
    dynClear(gNaviFwdHistory[2]);
  }

  dpSetWait(gConfigDp+".domain", domain, gConfigDp+".object", obj);
  dpSet(gConfigDp+".closeSecondary", true);
}

_fwFsmAtlas_updateHistory(string domain, string obj, dyn_string& history)
{
  if (dynlen(history) && history[dynlen(history)]==(domain+"|"+obj)) {
    // 		DebugTN(domain+"|"+obj+" was already at end of history!");
    return;
  }
  dynAppend(history, domain+"|"+obj);
  if (dynlen(history) == MAXHISTORY) dynRemove(history, 1);
}

/*
  open panel for given FSM object in main or secondary module of AtlasFrame
  and place navigator widget, any previous panels in the modules get
  overwritten
*/
fwFsmAtlas_openPanel(string domain, string obj, string topObj="", int isMain=1)
{
  //  	DebugTN("openPanel()");
  string panel, type;

  // 	DebugTN("getUserPanel()");
  fwUi_getUserPanel(domain, obj, panel);
  strreplace(panel, ".pnl", "");
  // 	DebugTN("getType()");
  fwCU_getType(domain+"::"+obj, type);

  // 	DebugTN("nopanel if necessary");
  if (!isMain) strreplace(panel, "Main", "Secondary");
  strreplace(panel, "\\", "/");
  if (getPath(PANELS_REL_PATH, panel+(isMain?"":"_info")+".pnl")=="") panel = "fwAtlas"+(isMain?"Main":"Secondary")+"Panels/noPanel";

  // 	DebugTN("updating panel with "+panel+(isMain?"":"_info")+".pnl");
  string panelFile = panel+(isMain?"":"_info")+".pnl"; 
  RootPanelOnModule(panelFile,
		    (isMain?"Main":"Info"),
		    (isMain?"main":"info")+"Module",
		    makeDynString("$node:"+domain, "$obj:"+obj,
				  "$topObj:"+topObj,
				  "$isMainPanel:"+isMain));
  // 	DebugTN("updating the navigator..");
  RootPanelOnModule("fwFSMuser/fwAtlas"+(isMain?"":"Secondary")+"Navigator.pnl",
		    (isMain?"":"secondary")+"Navigator",
		    (isMain?"n":"secondaryN")+"aviModule",
		    makeDynString("$domain:"+domain, "$obj:"+obj,
				  "$topObj:"+topObj,
				  "$isMainPanel:"+isMain));
  // 	DebugTN("open panel done.");
}

/*
  connect the current panel to a watchdog funtion which checks
  for all FSM CU children to be included in the control,
  after they were DEAD and if the parent object has taken the FSM tree
*/
dyn_string deadUnits;
string modeDomain;
string controlHost = "pcatlgcs01";

fwFsmAtlas_connectWatchMode(string domain, string obj)
{
  if (getHostname() != controlHost) return;
  modeDomain = domain;
  dyn_string children = fwFsmUi_getChildrenCUs(domain);
  mapping pendingDPs;
  for (int i=1; i<=dynlen(children); ++i) {
    string obj = children[i];

    string modeObj = fwUi_getModeObj(domain, obj);
    string dp;
    fwUi_getDomainPrefix(modeObj, dp);
    if (!dpExists(dp)) {
      pendingDPs[obj] = dp;
      continue;
    }
    dpConnect("_fwFsmAtlas_watchMode", dp+".mode.modeBits:_online.._value");
    delay(2);
  }

  // wait for the inaccessable objects since the connection might come back
  //
  while (mappinglen(pendingDPs)>0) {

    // reverse iterate since remove has to be done at the back first
    //
    for (int i=mappinglen(pendingDPs); i==1; --i) {
      string obj = mappingGetKey(pendingDPs, i);
      string dp = pendingDPs[obj];
      if (!dpExists(dp)) continue;
      dpConnect("watchMode", dp+".mode.modeBits:_online.._value");
      mappingRemove(pendingDPs, obj); // remove dp if connected
    }
    delay(5);
  }
}

void _fwFsmAtlas_watchMode(string dp, bit32 statusBits)
{
  string domain = modeDomain;
  string obj = substr(dp, strpos(dp, ":")+1);
  strreplace(obj, "fwCU_", "");
  strreplace(obj, ".mode.modeBits:_online.._value", "");
  string state;
  fwUi_getCurrentState(domain, obj+"::"+obj, state);

  bit32 rootBits = fwFsmUi_getModeBits(domain, domain);
  int sendCommands = getBit(rootBits, FwSendCommandsBit);
  int usesStates = getBit(rootBits, FwUseStatesBit);
  int isOwner = getBit(rootBits, FwOwnerBit);

  int index = dynContains(deadUnits, obj);
  if (state=="DEAD") {
    if (index < 1) dynAppend(deadUnits, obj);
  }
  else if (index > 0) {
    dynRemove(deadUnits, index);
  }

  if (statusBits == 0) { // dead
  }
  else if (fwFsmUi_isFree(statusBits)) {
    if (index > 0) {
      if (sendCommands && usesStates && isOwner && (state!="DEAD")) {
	DebugN(obj+" has to be included!");
	fwFsmUi_setCUModeByName(domain, obj+"::"+obj, "Include");
      }
      else {
      }
    }
  }
}

/*
  callback functions that can be used in panels for updating textfield
  values and state colors
*/
refreshValue(string dpe, float value)
{
  string color, sTemp;

  sTemp = dpe;
  strreplace(sTemp, "_online.._value", "");
  dpGet(sTemp + "_alert_hdl.._act_state_color", color);
  this.text = value;
  this.backCol = color;
}
readOut(string node, string state)
{
  string color;
  fwCU_getStateColor(node, state, color);
  this.text = state;
  this.backCol = color;
}

//
//
//
string fwFsmAtlas_getNodeDPEName(string domain, string device)
{
  return domain+"|"+device+".tnode";
}

fwFsmAtlas_getNodeNameComponents(string nodeName,
				 string& domain, string& device)
{
  dyn_string objName = strsplit(nodeName,'|');
  domain = substr(objName[1], strpos(objName[1], ":")+1);
  device = substr(objName[2], 0, strpos(objName[2], "."));
  if (strpos(device, "STATUS_")==0) device = substr(device, 7, strlen(device)); // in case the callback originates from a status object
}

//
// Function to create an FSM node together with its STATUS object
// parameters:
//   parent:       name of the parent object
//   name:         name of the object, if it is a reference use "domain::object"
//   objectFlag:   0 - LU, 1 - CU, 2 - DU
//   isReference:  the object to be added is a reference
//   system:       for cross-system references: system name where the referenced object is located
//   checkExists:  if node is already existing, do not create it
//
bool fwFsmAtlas_createNode(string parent, string name, string type,
			   string label = "", string panelName = "",
			   int objectFlag = 1, bool isReference = false, string system = "",
			   bool checkExists = false)
{
  DebugTN("fwFsmAtlas_createNode: "+parent+", "+name+", "+type+", "+label+", "+panelName+", "+objectFlag+", "+isReference+", "+system);

  bool doNotCreate = false;
  if (checkExists && dpExists("fwTN_"+name)) {
    warning("Node exists already, ignoring creation and just apply label and panel settings.");
    doNotCreate = true;
  }
  // check that the call was valid
  //
  string parentDomain = parent;
  string parentObject = parent;
  if (strpos(parent, "::")>-1) {
    parentDomain = fwFsm_getAssociatedDomain(parent);
    parentObject = fwFsm_getAssociatedObj(parent);
  }
  if (parentDomain=="" || parentObject=="") {
    error("fwFsmAtlas_createNode: invalid parent object name "+parent);
    return false;
  }
  if (!dpExists("fwTN_"+parentDomain)) {
    error("fwFsmAtlas_createNode: cannot determine "
	  +"the domain of parent non-CU object "+parent);
    return false;
  }

  if (system!="") system = system + ":";
  DebugTN("Create node ", parent, name, type, label, panelName, objectFlag);
  if (!doNotCreate) fwFsmTree_addNode(parent, system+name, type, (objectFlag==1));
  if (panelName=="") panelName = name;
  strreplace(panelName, ".pnl", "");
  fwFsmTree_setNodePanel(name, "fwAtlasMainPanels/"+panelName+".pnl");
  if (label!="") fwFsmTree_setNodeLabel(name, label);

  //
  // create STATUS object
  // - for LU/CU as child of the object itself
  // - for DU as child of parent STATUS object
  //
  if (!isReference) {
    DebugTN("Create status node ", (objectFlag==1) ? name : "STATUS_"+parentObject,
	    "STATUS_"+name, (objectFlag!=2) ? "ATLAS_STATUS" : "ATLAS_DU_STATUS", 0);
    if (!doNotCreate) fwFsmTree_addNode( (objectFlag==1) ? name : "STATUS_"+parent,
					 "STATUS_"+name,
					 (objectFlag!=2) ? "ATLAS_STATUS" : "ATLAS_DU_STATUS", 0);
  }

  //
  // in case the object is not a DU or a top node of the system
  // create a reference of its STATUS object as a child
  // of the parent STATUS object
  //
  if ((objectFlag==1) && parent!="FSM") {
    string statusNode;
    if (!isReference) statusNode = system + ( (objectFlag==1) ? name : parentDomain ) + "::STATUS_" + name;
    else {
      string nodeDomain = name;
      string nodeObject = name;
      if (strpos(name, "::")>-1) {
	nodeDomain = fwFsm_getAssociatedDomain(name);
	nodeObject = fwFsm_getAssociatedObj(name);
      }
      statusNode = system + nodeDomain+"::STATUS_" + nodeObject;
    }
    DebugTN("Create status node ", "STATUS_" + parent, statusNode, "ATLAS_STATUS", 0);
    if (!doNotCreate) fwFsmTree_addNode("STATUS_" + parentObject, statusNode, "ATLAS_STATUS", 0);
  }
}





/**
   It gets all the trees in the hierarchy. In order it it reach the most deeper
   node before scanning the next
   dyn_string &allChildren -> result with the name of all nodes
   dyn_string &allFlags -> result with the flags of all nodes. Flags 1=CU, 2=DU, 0=LU
   string topNode -> Input, if empty "" means root nodes.
*/
fwFsmAtlasGetTreeNodes(dyn_string &allChildren, dyn_int &allFlags, string topNode)
{
  int i;
  dyn_string exInfo, children;
  dyn_int flags;

  if (topNode == "")
    {
      fwTree_getChildren("FSM", children, exInfo);
      // set root nodes
      for (i = 1; i<= dynlen(children); i++)
	{
	  allChildren[i] = children[i];
	  allFlags[i] = 1;
	}
    }
  else if (fwFsmTree_isNode(topNode))
    {
      dynInsertAt(allChildren, topNode, 1);
      dynInsertAt(allFlags, 1, 1);
    }
  else
    {
      dynClear(allChildren);
      dynClear(allFlags);
      DebugN("The specified node " + topNode + " doesn't exist. Please, check the entry name again.");
    }
  for(i=1; i <= dynlen(allChildren); i++)
    {
      children = fwCU_getChildren(flags, allChildren[i], "");
      dynInsertAt(allChildren, children, i+1);
      dynInsertAt(allFlags, flags, i+1);
    }
}

/**
   It creates all status objects for a hierarchy completely empty of this objects.
   If topNode = "" the status objects are added for all hierarchies.
   If topNode is a node in the hierarchy the function adds the status object from this node down
*/
fwFsmAtlasAddAllStatusObjects(string statusObjType, string statusDuType, string topNode)
{
  int i, isCU;
  dyn_string allChildren, checkStatus;
  dyn_int allFlags;
  string parent;

  fwFsmAtlasGetTreeNodes(allChildren, allFlags, topNode);

  checkStatus = dynPatternMatch("*STATUS_*" , allChildren);
  if (dynlen(checkStatus) > 1)
    {
      DebugN("There were already status objects declared. Please, delete all status objects before using function fwFsmAtlasAddAllStatusObjects");
    }
  else
    {
      for(i=1; i <= dynlen(allChildren); i++)
	{
	  parent = fwCU_getParent(isCU, allChildren[i]);  // isCU 1=CU, 0=LU

	  if ((parent == "") && (fwFsmTree_isNode(allChildren[i]))) // is root node
	    {
	      fwFsmTree_addNode(allChildren[i], "STATUS_" + allChildren[i], statusObjType, 0);
	      DebugN("STATUS_" + allChildren[i] + " added");
	    }
	  else if (allFlags[i] == 1) // is CU
	    {
	      fwFsmTree_addNode(allChildren[i], "STATUS_" + allChildren[i], statusObjType, 0);
	      fwFsmTree_addNode("STATUS_" + parent, allChildren[i] + "::STATUS_" + allChildren[i], statusObjType, 0);
	      DebugN("STATUS_" + allChildren[i] + " added and referenced within STATUS_" + parent);
	    }
	  else if (allFlags[i] == 0) // is LU
	    {
	      if (!isCU)
		DebugN("Error creating the hierarchy. LU being children of another LU not allowed");
	      else
		{
		  fwFsmTree_addNode(allChildren[i], "STATUS_" + allChildren[i], statusObjType, 0);
		  fwFsmTree_addNode("STATUS_" + parent, allChildren[i] + "::STATUS_" + allChildren[i], statusObjType, 0);
		  DebugN(parent+"::STATUS_" + allChildren[i] + " added and referenced within STATUS_" +  parent);
		}
	    }
	  else // is DU
	    {
	      fwFsmTree_addNode(parent, "STATUS_" + allChildren[i], statusDuType, 0);
	      fwFsmTree_addNode("STATUS_" + parent, parent + "::STATUS_" + allChildren[i], statusDuType, 0);
	      DebugN("STATUS_" + allChildren[i] + " added and referenced within STATUS_" + parent);
	    }
	}
      if (topNode == "")
	fwFsmTree_generateAll();
      else
	fwFsmTree_generateTreeNode(topNode, 1, "");
    }
  fwFsmTree_refreshTree();
}

/**
   It removes all status objects for a hierarchy.
   If topNode = "" the status objects are removed from all hierarchies.
   If topNode is a node in the hierarchy the function removes the status object from this node down.
   Still a problem when removing the referenced status object.
   fwFsmTree_removeNode("STATUS_" + parent, parent + "::STATUS_" + node, 0) doesn't work properly yet
   Recommendation, for the time being using it for all the hierarchy. So, topNode = ""
*/
fwFsmAtlasRemoveAllStatusObjects(string topNode)
{
  int i, isCU;
  dyn_string allChildren;
  dyn_int allFlags;
  string parent;

  fwFsmAtlasGetTreeNodes(allChildren, allFlags, topNode);

  for(i=1; i <= dynlen(allChildren); i++)
    {
      if ((patternMatch("*STATUS_*" , allChildren[i])) && (fwFsmTree_isNode(allChildren[i])))
	{
	  parent = fwCU_getParent(isCU, allChildren[i]);  // isCU 1=CU, 0=LU
	  if (isCU)
	    fwFsmTree_removeNode(parent, allChildren[i], 1);
	  else
	    fwFsmTree_removeNode(parent, allChildren[i], 0);
	  DebugN(allChildren[i] + " removed from tree");
	  dynRemove(allChildren, i);
	  dynRemove(allFlags, i);
	  i = i-1;
	}
    }
  /**	parent = fwCU_getParent(isCU, topNode);  // isCU 1=CU, 0=LU
     if ((topNode != "")&&(fwFsmTree_isNode("STATUS_" + parent)))
     {
     sys = getSystemName();
     fwFsmTree_removeNode("STATUS_" + parent, topNode + "::STATUS_" + topNode, 0);				// remove reference
     DebugN(topNode + "::STATUS_" + topNode + " removed from tree");
     }*/
  if (topNode == "")
    fwFsmTree_generateAll();
  else
    //	fwFsmTree_generateTreeNode(parent, 1, "");
    fwFsmTree_generateTreeNode(topNode, 1, "");
  fwFsmTree_refreshTree();
}

/** It associates a status object to a certain node.
    It checks if status objects below in the hierarchy exist already. If so, the status object is not created
    in order to avoid a mess.
    This function should be used within the script that creates the hierarchy from top to down. So when using fwFsmTree_addNode to add
    a node, afterwards using fwFsmAtlasAddStatusObject it will associate the appropiate status object.
    string statusObjType -> Type of the status object to be added
    string node -> Name of the node which an status object will be associated
    int flag -> Type of the node which an status object will be associated. Flags 1=CU, 2=DU, 0=LU
*/
fwFsmAtlasAddStatusObject(string statusObjType, string node, int flag)   // Flags 1=CU, 2=DU, 0=LU
{
  int i, isCU;
  dyn_string allChildren, checkStatus;
  dyn_int allFlags;
  string parent;

  fwFsmAtlasGetTreeNodes(allChildren, allFlags, node);

  checkStatus = dynPatternMatch("*STATUS_*" , allChildren);
  if (dynlen(checkStatus) > 1)
    {
      DebugN("There was already a status objects declared below on the hierarchy. Please, delete all status objects located below before using function fwFsmAtlasStatusObjects",checkStatus);
    }
  else
    {
      parent = fwCU_getParent(isCU, node);  // isCU 1=CU, 0=LU

      if ((parent == "") && (fwFsmTree_isNode(allChildren[i]))) // is root node
	{
	  fwFsmTree_addNode(node, "STATUS_" + node, statusObjType, 0);
	  DebugN("STATUS_" + node + " added");
	}
      else if (flag == 1) // is CU
	{
	  fwFsmTree_addNode(node, "STATUS_" + node, statusObjType, 0);
	  fwFsmTree_addNode("STATUS_" + parent, node + "::STATUS_" + node, statusObjType, 0);
	  DebugN("STATUS_" + node + " added and referenced within STATUS_" + parent);
	}
      else if (flag == 0) // is LU
	{
	  if (!isCU)
	    DebugN("Error creating the hierarchy. LU being children of another LU not allowed");
	  else
	    {
	      fwFsmTree_addNode(node, "STATUS_" + node, statusObjType, 0);
	      fwFsmTree_addNode("STATUS_" + parent, node + "::STATUS_" + node, statusObjType, 0);
	      DebugN("STATUS_" + node + " added and referenced within STATUS_" +  parent);
	    }
	}
      else // is DU
	{
	  fwFsmTree_addNode(parent, "STATUS_" + node, statusObjType, 0);
	  fwFsmTree_addNode("STATUS_" + parent, parent + "::STATUS_" + node, statusObjType, 0);
	  DebugN("STATUS_" + node + " added and referenced within STATUS_" + parent);
	}
      if (node == "")
	fwFsmTree_generateAll();
      else if (parent == "")
	fwFsmTree_generateTreeNode(node, 1, "");
      else
	fwFsmTree_generateTreeNode(parent, 1, "");
    }
}

/** It removes a status object from a certain node.
    This function should be used very often. Still a problem when removing the referenced status object.
    fwFsmTree_removeNode("STATUS_" + parent, parent + "::STATUS_" + node, 0) doesn't work properly yet
*/
fwFsmAtlasRemoveStatusObject(string node, int flag)
{
  int i, isCU, isCU2;
  dyn_string allChildren;
  dyn_int allFlags;
  string parent;

  if (fwFsmTree_isNode("STATUS_" + node))
    {
      parent = fwCU_getParent(isCU, node);  // isCU 1=CU, 0=LU

      if ((flag == 1)&&(parent == "")) // is root node
	{
	  fwFsmTree_removeNode(node, "STATUS_" + node, 1);
	  DebugN("STATUS_" + node + " removed from tree");
	}
      else if (flag == 2)  // Flags 2=DU
	{
	  fwFsmTree_removeNode(parent, "STATUS_" + node, 1);
	  DebugN("STATUS_" + node + " removed from tree");
	  /** 			if (fwFsmTree_isNode("STATUS_" + parent) && (fwFsmTree_isNode(parent + "::STATUS_" + node)))
	      {
	      fwFsmTree_removeNode("STATUS_" + parent, parent + "::STATUS_" + node, 0);				// remove reference
	      DebugN("Reference" + parent + "::STATUS_" + node + " removed from tree");
	      }*/
	}
      else  // Flags 1=CU, 0=LU
	{
	  fwFsmTree_removeNode(node, "STATUS_" + node, 1);
	  DebugN("STATUS_" + node + " removed from tree");
	  /** 			if (fwFsmTree_isNode("STATUS_" + parent) && (fwFsmTree_isNode(node + "::STATUS_" + node)))
	      {
	      fwFsmTree_removeNode("STATUS_" + parent, "STATUS_" + node, 0);				// remove reference
	      DebugN("Reference STATUS_" + node + " removed from tree");
	      }	*/
	}
      if (node == "")
	fwFsmTree_generateAll();
      else
	fwFsmTree_generateTreeNode(node, 1, "");
      fwFsmTree_refreshTree();
    }
  else
    DebugN("STATUS_" + node + " doesn't exist. It cannot be removed. ");
}

/**
   Creates the STATUS structure for a given domain
   The CU should not contain any other CUs as children further down its tree
   nor should it contain cross-system references!
*/

void fwFsmAtlas_createStatusSubTree(string domain, string object, bool createTop = true)
{
  if (createTop) {
    string node = fwFsmTree_addNode(object, "STATUS_"+object, "ATLAS_STATUS", 0);
    DebugTN("Created node "+node);
  }

  dyn_int flags;
  dyn_string children = fwFsm_getObjChildren(domain, object, flags);
  DebugTN(children,flags);
  for (int i=1; i<=dynlen(children); ++i) {
    DebugTN("processing "+children[i]+":"+flags[i], strpos(children[i], "STATUS_"));
    if (flags[i]==1) {
      DebugTN("Ups, there are subdomains existing! This should not happen!!");
      break;
    }
    string childObject = children[i];
    if (strpos(childObject, "STATUS_")>-1) continue; // ignore STATUS children
    string statusType = "ATLAS_STATUS";
    if (flags[i]==2) statusType = "ATLAS_DU_STATUS"; // node is a DU
    DebugTN("Creating "+"STATUS_"+childObject+" as child of "+domain+"::"+"STATUS_"+object+" with type "+statusType+" ...");
    string node = fwFsmTree_addNode("STATUS_"+object, "STATUS_"+childObject, statusType, 0);
    DebugTN("Created node "+node);
    if (flags[i]==1) processNode(domain, childObj, false); // node is a LU, process children
  }
}


fwFsmAtlas_loggedUserAccessControlCallback(string s1, string s2)
{
  //	fwUi_loggedUserAccessControlCallback(s1, s2);
  dpSetWait(gConfigDp+".closeFsmModule", true);
  dpSetWait(gConfigDp+".closeSecondary", true);
}


mapping fwFsmAtlas_timeoutTimers;

fwFsmAtlas_startTimeout(int secs, string domain, string device, string timeout_state = "", string desired_state = "")
{
  startThread("_fwFsmAtlas_startTimeout", secs, domain, device, timeout_state, desired_state);
}

_fwFsmAtlas_startTimeout(int secs, string domain, string device, string timeout_state = "", string desired_state = ""){

  string dev = fwDU_checkLogicalName(domain, device);
  string dp = getSystemName()+domain+fwFsm_separator+dev+fwDU_getFsmMidfix()+".currentState:_online.._value";

  // check 1st if desired state already reached
  string state;
  dpGet(dp, state);
  if (desired_state!="" && state==desired_state) {
    warning("Desired state already reached for "+domain+"::"+device+". No timeout waiting initiated.");
    return;
  }

  if (mappingHasKey(fwFsmAtlas_timeoutTimers, dp)) {
    error("Timeout already used for "+domain+"::"+device);
  }
  fwFsmAtlas_timeoutTimers[dp] = makeDynAnytype(secs, "");
  
  dpConnect("fwFsmAtlas_timeoutStateChange", false, dp);

  while (fwFsmAtlas_timeoutTimers[dp][1]>0) {
    --fwFsmAtlas_timeoutTimers[dp][1];
    delay(1); 
  }
  dpDisconnect("fwFsmAtlas_timeoutStateChange", dp);

  int timer = fwFsmAtlas_timeoutTimers[dp][1];
  if (timer==0) { // timed out
    warning("Timeout of "+secs+"s for "+domain+"::"+device+" expired.");
    if (timeout_state=="") { // just touch state value in dpe
      DebugTN("Refreshing state of "+domain+"::"+device+" after timeout.");
      if (strpos(dp, ":_online.._value")>-1) {
        dp = substr(dp, 0, strlen(dp)-strlen(":_online.._value")); }
      string state;      
      dpGet(dp, state);
      dpSetWait(dp, state);
      dp = dp+":_online.._value";
    }
    else {
      DebugTN("Setting timeout state "+timeout_state+" to "+domain+"::"+device+" after timeout.");
      if (strpos(dp, ":_online.._value")>-1) {
        dp = substr(dp, 0, strlen(dp)-strlen(":_online.._value")); }      
      dpSetWait(dp, timeout_state);
      dp = dp+":_online.._value";      
    }
  }
  else if (timer==-1) { // state changed or got refreshed
    DebugTN("Got callback for "+domain+"::"+device+", canceling timeout.");
  }
  else error("fwFsmAtlas_startTimeout(): Impossible timer value:"+timer);

  mappingRemove(fwFsmAtlas_timeoutTimers, dp);
}

void fwFsmAtlas_timeoutStateChange(string dp, string state)
{
  fwFsmAtlas_timeoutTimers[dp] = makeDynAnytype(-1, state); 
}

string fwFsmAtlas_getTwikiHelpMenuItem(string domain,string object)
{
  string menuItem,type,sys;
  fwUi_getDomainSys(domain,sys);
  string subdet = fwAtlas_getSubdetectorId(sys);
  string identifier = (domain==object) ? domain : domain+"::"+object;
  
  if(domain==object)
    fwFsm_getObjectType(domain + "::" + identifier,type);
  else
    fwFsm_getObjectType(identifier,type);
  
  //Find if node is set as exception as node or as FSM type
  dyn_string typeExceptions,nodeExceptions;
  dpGet("ATLGCSOI2:" + subdet+ "_twikiExceptions.types",typeExceptions);
  dpGet("ATLGCSOI2:" + subdet+ "_twikiExceptions.nodes",nodeExceptions);
  bool exception = false;
  if(dynlen(typeExceptions) > 0){
    for(int i = 1; i<= dynlen(typeExceptions); i++){
      if(type == typeExceptions[i]){
        exception = true;
        break;
      }
    }
  }
  if(dynlen(nodeExceptions) > 0 && exception == false){
    for(int i = 1; i<= dynlen(nodeExceptions); i++){
      if(identifier == nodeExceptions[i] || identifier == nodeExceptions[i] +"::"+ nodeExceptions[i]){
        exception = true;
        break;
      }
    }
  }
  
  //If not already an exception, check if the node is part of the 3-layer menu (must be returned as node name)
  bool inMenu = FALSE;
  if(!exception){
    dyn_string nodes;
    dpGet("ATLGCSOI2:" + subdet + "_nodeList.nodes",nodes);
    DebugN(subdet,nodes);
    if(dynContains(nodes,identifier) > 0)
      inMenu = TRUE;
  }
  
  //resolve menu item
  if(!exception && !inMenu){
    menuItem = subdet + "_fsm_" + type;
  }
  else {
    strreplace(identifier, "::", "__");
    menuItem = subdet + "_fsm_" + identifier;
  }
  
  return menuItem;
}

// Transform framework color name to ATLAS Status
//
// @author pnikiel
// @date May-2016
bool fwFsmAtlas_colorNameToStatus (string colorName, string&  status)
{
  if (patternMatch ("*Warn*", colorName))
  {
    status = "WARNING";
    return true;
  }
  else if (patternMatch ("*Error*", colorName))
  {
    status = "ERROR";
    return true;
  }
  else if (patternMatch ("*Fatal*", colorName))
  {
    status = "FATAL";
    return true;
  }
  else if (colorName=="" || colorName=="FwStateOKPhysics" || colorName=="FwStateOKNotPhysics") // no alert
  {
    status = "OK";
    return true;
  }
  else
  {
    error("This color name cannot be converted to status:"+colorName);
    return false;
  }
}

bool _fwFsmAtlas_statusToSeverity( string status, int& output )
{
    switch(status)
    {
	case "OK": output = 0; return true;
	case "WARNING": output = 1; return true;
	case "ERROR": output = 2; return true;
	case "FATAL": output = 3; return true;
	default: error("_fwFsmAtlas_statusToSeverity: status unrecognized: "+status); return false;
    }
}

bool _fwFsmAtlas_severityToStatus( int severity, string& output )
{
    switch(severity)
    {
	case 0: output = "OK"; return true;
	case 1: output = "WARNING"; return true;
	case 2: output = "ERROR"; return true;
	case 3: output = "FATAL"; return true;
	default: return false;
    }
}

// From list of statuses passed get the most severe one. Returns true if and only if evaluation was successful
//
// @author pnikiel
// @date May-2016
bool fwFsmAtlas_getMostSevereStatus (dyn_string statuses, string& output)
{
  int currentlyMostSevere = 0;
  for (int i=1; i<=dynlen(statuses); i++)
  {
    int tmpSeverity = 0;
    if (! _fwFsmAtlas_statusToSeverity( statuses[i], tmpSeverity ) )
    {
      DebugN ("this status not evaluated to severity: "+statuses[i] );
      return false;
    }
    if (tmpSeverity > currentlyMostSevere)
      currentlyMostSevere = tmpSeverity;
  }
  if (!_fwFsmAtlas_severityToStatus( currentlyMostSevere, output ))
     return false;
  return true;
}
