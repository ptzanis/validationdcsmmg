//
// Color defines
//
const string ATLAS_BUTTON_BGCOLOR = "FwAtlasFg";

// Atlas Control Rom
//
const string DCS_DESK_MACHINE = "pc-atlas-cr-dcs";

// Location of Atlas software repository
//
string REPOSITORY_WIN = "//atlas-storage-det-dcs/DCS/Production/";
string REPOSITORY_LIN = "/det/dcs/Production/";

// DCS information Servers
//
const string DCS_IS_DSS = "ATLGCSIS1";
const string DCS_IS_GAS = "ATLGCSIS2";
const string DCS_IS_MAGNETS = "ATLGCSIS3";
const string DCS_IS_CRYO = "ATLGCSIS3";
const string DCS_IS_CAV = "ATLGCSIS3";
const string DCS_IS_LHC = "ATLGCSLHC";
const string FW_CONDDB_RDB_ARCHIVE_CLASS = "RDB-99) EVENT";

// DIM DNS Servers
const string FSMDNS = "pcatlgsmdns1,pcatlfsmdns2";
const string LOGDNS = "pcatllogdns1";
const string FMCDNS = "pcatlfmsdns1";
const string DDCDNS = "pcatlddcdns1";
const string FSMDNSTEST = "pcatltestdns1";
const string LOGDNSTEST = "pcatltestlogdns1";

void fwAtlas()
{}

/*
	Wait until all distributed systems are connected within given timeout
*/

bool fwAtlas_waitForDistConnections(int timeout=30)
{
  dyn_string entries;
  dyn_int nominalSystems;
  
  // Read the IDs of the systems that should be connected from the config file
  //
  paCfgReadValueList(getPath("config", "config"), "", "distPeer", entries, "distPeer");
  //	DebugTN(entries);
  for (int i=1; i <= dynlen(entries); ++i) {
    string entry = entries[i];
    dyn_string items = strsplit(entry, ' ');
    int id;
    if (dynlen(items)<2) continue;
    sscanf(items[2],"%d",id);
    dynAppend(nominalSystems, id);
  }
  DebugTN("List of the systems that should be connected: ",nominalSystems );
  
  // Wait for the list of current connections to match with the config file's list
  //
  int i=0;
  for(int s=1; s<=dynlen(nominalSystems); ++s) {
    for(; i<timeout; ++i) {
      bool connected;
      unDistributedControl_isConnected(connected, getSystemName(nominalSystems[s]));
      if (!connected) delay(1);
      else break;
    }
  }
  DebugTN(i);
  if (i<timeout) {
    DebugTN("All systems connected after "+i+" seconds.");
    delay(2); // FIXME: wait for dist connections reach managers when just connected
    return true;
  }

  DebugTN("Not all systems are connected after timeout");
  
  // print not connected systems
  //
  dyn_int sysNumsConnected;
  dpGet("_DistManager.State.SystemNums", sysNumsConnected);
  dyn_int sysMissing;
  for (int s=1; s<=dynlen(nominalSystems); ++s) {
    if (!dynContains(sysNumsConnected, nominalSystems[s])) dynAppend(sysMissing, nominalSystems[s]);
  }
  fatal("The following systems were not connected after the timeout of "+i+" seconds: "+sysMissing);
  return false;
}

string fwAtlas_getSubdetectorId(string currentSys)
{
  string id; 
  id = substr(currentSys, 3, 3);
  strreplace(currentSys, ":", "");
  if(id == "GCS") {
    if(currentSys == "ATLGCSIS1") id = "SAF";
    else if(currentSys == "ATLGCSLUMI" || currentSys == "ATLGCSLHC") id = "LHC";
    else if(strpos(currentSys, "ATLGCSIS") == 0) id = "EXT";
  }
  return(id);
}


string fwAtlas_getSubdetectorACdomain(string currentSys)
{
  string acDomain = fwAtlas_getSubdetectorId(currentSys);
  //handle exceptions 
  switch (acDomain) {
    case "SAF": acDomain = "GCS";break;
    case "EXT": acDomain = "GCS";break;
    default: break;
  }
  DebugTN("Considering AC domain "+acDomain+" for "+currentSys);
  return acDomain;
}


/*
  
  ATLAS message classes
  Should be used in correspondance to Alerts or FSM STATUS
  
*/

information(string body, string fsmNode="")
{
  errClass retError = (fsmNode=="") ?
    makeError("atlasMessages", PRIO_INFO, ERR_CONTROL, 1001, body)
    : makeError("atlasMessages", PRIO_INFO, ERR_CONTROL, 1001, fsmNode+": "+body);
  throwError(retError);
  DebugFTN("ATLASMESSAGES", "INFO: "+body);
}

action(string body, string fsmNode="")
{
  errClass retError = (fsmNode=="") ?
    makeError("atlasMessages", PRIO_INFO, ERR_CONTROL, 1002, body)
    : makeError("atlasMessages", PRIO_INFO, ERR_CONTROL, 1002, fsmNode+": "+body);
  throwError(retError);
  DebugFTN("ATLASMESSAGES", "ACTION: "+body);
}

debug(string body, string fsmNode="")
{
  errClass retError = (fsmNode=="") ?
    makeError("atlasMessages", PRIO_INFO, ERR_CONTROL, 1005, body)
    : makeError("atlasMessages", PRIO_INFO, ERR_CONTROL, 1005, fsmNode+": "+body);
  throwError(retError);
  DebugFTN("ATLASMESSAGES", "DEBUG: "+body);
}

warning(string body, string fsmNode="")
{
  errClass retError = (fsmNode=="") ?
    makeError("atlasMessages", PRIO_WARNING, ERR_CONTROL, 1040, body)
    : makeError("atlasMessages", PRIO_WARNING, ERR_CONTROL, 1040, fsmNode+": "+body);
  throwError(retError);
  DebugFTN("ATLASMESSAGES", "WARNING: "+body);
}

error(string body, string fsmNode="")
{
  errClass retError = (fsmNode=="") ?
    makeError("atlasMessages", PRIO_SEVERE, ERR_CONTROL, 1060, body)
    : makeError("atlasMessages", PRIO_SEVERE, ERR_CONTROL, 1060, fsmNode+": "+body);
  throwError(retError);
  DebugFTN("ATLASMESSAGES", "ERROR: "+body);
}

fatal(string body, string fsmNode="")
{
  errClass retError = (fsmNode=="") ?
    makeError("atlasMessages", PRIO_SEVERE, ERR_CONTROL, 1080, body)
    : makeError("atlasMessages", PRIO_SEVERE, ERR_CONTROL, 1080, fsmNode+": "+body);
  throwError(retError);
  DebugFTN("ATLASMESSAGES", "FATAL: "+body);
}

///////////////////////////////////////////////////////////////////////////////


dyn_string ex;

bool fwAtlas_checkException(string errorText, dyn_string& exception)
{
  if (dynlen(exception)>0) {
    error(">>>>> Exception: "+errorText, exception);
    dynClear(exception);
    return true;
  }
  return false;
}

fwAtlas_showAccessDenied(string needPriv = "")
{
  string text = "You don't have sufficient priviliges for this operation:"+needPriv;
  warning(text);
  fwAtlas_popup(text);
}

fwAtlas_popup(string message)
{
  dyn_float retFloat;
  dyn_string retString;
  ChildPanelOnReturn("vision/MessageWarning", "okDialog", makeDynString("$1:"+message),
                     600, 500, retFloat, retString);
}

bool fwAtlas_isDescriptionValid(string description)
{
  int maxLength = 100;
  if (strlen(description)>=maxLength) {
    error("Descriptions must be shorter than +"+maxLength+" characters. Please correct "+description);
    return false;
  }
  if (strpos(description, " ")!=3) {
    error("Descriptions must contain the subdetector/subsystem identifier as the first 3 characters. Please correct "+description);
    return false;
  }
  if (strpos(description, "(")>-1 || strpos(description, ")")>-1
     || strpos(description, "/")>-1 || strpos(description, ".")>-1
     || strpos(description, "&")>-1 || strpos(description, "$")>-1
     || strpos(description, ":")>-1 || strpos(description, ";")>-1
     || strpos(description, ",")>-1 || strpos(description, "%")>-1) {
    error("Descriptions are not allowed to contain special characters other than spaces. Please correct "+description);
    return false;
  }
  return true;
}

fwAtlas_setArchiveConfig(dyn_string dpes, int archiveType = DPATTR_ARCH_PROC_SIMPLESM, int smoothProcedure = DPATTR_TIME_SMOOTH, float deadband = 1800, float timeInterval = 1800)
{    
  // Set the archive configs
  information("Set archive config for "+dpes);
  fwArchive_setMultiple(dpes, FW_CONDDB_RDB_ARCHIVE_CLASS,
			archiveType, smoothProcedure, deadband,
			timeInterval, ex);
  fwAtlas_checkException("Exception during setting archive config:", ex);
}


///////////////////////////////////////////////////////////////////////////////
//
// Mutex implementation functions
// usage:
//   - first init mutex with name: fwAtlasMutex_initLock(name)
//   - then lock or unlock mutex with fwAtlasMutex_lock(name)
//     or fwAtlasMutex_unlock(name)
//
///////////////////////////////////////////////////////////////////////////////

synchronized bool fwAtlasMutex_initLock(string lockName)
{
  if (!globalExists("gAtlasMutexList")) {
    addGlobal("gAtlasMutexList", DYN_STRING_VAR);
    addGlobal("gAtlasMutexLocked", DYN_BOOL_VAR);
  }
  if (dynlen(gAtlasMutexList) == 10) {
    error("Cannot create more mutex locks, maximum reached (10).");
    return false;
  }

  if (dynContains(gAtlasMutexList, lockName)<=0) {
    dynAppend(gAtlasMutexList, lockName);
    dynAppend(gAtlasMutexLocked, false);
  }
  DebugTN("fwAtlasMutex_initLock "+lockName, gAtlasMutexList, gAtlasMutexLocked);
  return true;
}

fwAtlasMutex_lock(string lockName)
{
  //  DebugTN("fwAtlasMutex_lock: lock "+lockName);
  int index = dynContains(gAtlasMutexList, lockName);
  //  DebugTN("fwAtlasMutex_lock: lock "+lockName+" has index "+index);
  switch (index) {
  case 1: _fwAtlasMutex_lock1();break;
  case 2: _fwAtlasMutex_lock2();break;
  case 3: _fwAtlasMutex_lock3();break;
  case 4: _fwAtlasMutex_lock4();break;
  case 5: _fwAtlasMutex_lock5();break;
  case 6: _fwAtlasMutex_lock6();break;
  case 7: _fwAtlasMutex_lock7();break;
  case 8: _fwAtlasMutex_lock8();break;
  case 9: _fwAtlasMutex_lock9();break;
  case 10: _fwAtlasMutex_lock10();break;
  }
}

synchronized fwAtlasMutex_unlock(string lockName)
{
  int index = dynContains(gAtlasMutexList, lockName);
  gAtlasMutexLocked[index] = false;
}

_lock(int lockNum)
{
//   DebugTN("_lock: lock "+lockNum+gAtlasMutexLocked);
  while (gAtlasMutexLocked[lockNum]) {
    //    DebugTN("_lock: waiting for getting lock "+lockNum);
    delay(0, 50);
  }
  gAtlasMutexLocked[lockNum] = true;
}

//
// need one lock function per mutex (max = 10) to protect lock list
//
synchronized _fwAtlasMutex_lock1() {_lock(1);}
synchronized _fwAtlasMutex_lock2() {_lock(2);}
synchronized _fwAtlasMutex_lock3() {_lock(3);}
synchronized _fwAtlasMutex_lock4() {_lock(4);}
synchronized _fwAtlasMutex_lock5() {_lock(5);}
synchronized _fwAtlasMutex_lock6() {_lock(6);}
synchronized _fwAtlasMutex_lock7() {_lock(7);}
synchronized _fwAtlasMutex_lock8() {_lock(8);}
synchronized _fwAtlasMutex_lock9() {_lock(9);}
synchronized _fwAtlasMutex_lock10() {_lock(10);}


///////////////////////////////////////////////////////////////////////////////

bool fwAtlas_isTtcPartitionName(string name)
{
  if (!globalExists("gAtlasTtcPartitions")) {
    addGlobal("gAtlasTtcPartitions", DYN_STRING_VAR);
    gAtlasTtcPartitions = makeDynString(
	"BARREL", "BLAYER", "DISKS", "IBL",
        "BAR", "ECA", "ECC",
        "BARREL", "BARRELA", "BARRELC", "ENDCAPA", "ENDCAPC",
        "EMBA", "EMBC", "EMECA", "EMECC", "HECFCALA", "HECFCALC",
        "EBA", "EBC", "LBA", "LBC",
        "BA", "BC", "EA", "EC",
        "A", "C",
        "A", "C",
        "A", "C"
        );
  }
  
  if (dynContains(gAtlasTtcPartitions, name)>0) return true;
  return false;
  
}

bool fwAtlas_isSubdetName(string name)
{
  if (!globalExists("gAtlasSubdets")) {
    addGlobal("gAtlasSubdets", DYN_STRING_VAR);
    gAtlasSubdets = makeDynString(
        "PIX", "SCT", "TRT", "IDE",
        "LAR", "TIL",
        "MDT", "TGC", "RPC", "CSC", "MUO",
        "CIC", "TDQ", "EXT", "SAF", "LHC", "LUM",
	"FWD", "LCD", "ZDC", "BCM", "RPO", "AFP"
        );
  }
  
  if (dynContains(gAtlasSubdets, name)>0) return true;
  return false;
  
}

bool fwAtlas_inVLAN()
{
  bool inATCN = true;
  string pcName = _WIN32 ? getenv("COMPUTERNAME") : getenv("HOSTNAME");
  if (strpos(getHostByName(pcName), "10.")<0) inATCN=false;
  return inATCN;
}

fwAtlas_copyPathOpcUaCetificates()
{
// fwAtlas.config sets path to OPC UA certificates repo to /localdisk/winccoa/opcua/client  
  // we have to copy original PKI repository there, otherwise client wont be able to store certificates
  //  
  DebugTN("Now I'll try to copy the PKI Certificate Store to the new location.");
  string source, destination;
  if (paCfgReadValue(getPath(CONFIG_REL_PATH, "config"), "general", "pvss_path", source) == 0) {
    if (paCfgReadValue(getPath(CONFIG_REL_PATH, "config"), "opcua", "certificateStore", destination) == 0) {
      if (isdir(destination)==0) {
	// Directory doesnt exist, create
	if (mkdir (destination)==0) DebugTN ("Couldnt create target directory! "+destination);
	else {
	  if (copyAllFilesRecursive (source+"/data/opcua/client", destination)==1) {
	    DebugTN("PKI Certificate Store copied to the new location: "+destination);
	  }
	  else DebugTN("Couldnt copy the certificate store to the new location. Do it manually."); 
	}
      }
      else DebugTN ("Directory exists already; not overwriting!");
    }
    else DebugTN("certificateStore not found in your config file... strange!");
  }
  else DebugTN ("Failed: Couldn't read pvss_path from your config file.");

}
