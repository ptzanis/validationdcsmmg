/**@file

This library contains utility functions for the export of fwDIP config.

@par Creation Date
	19/12/2002

@par Modification History
  09/04/2015: Herve
  ENS-11382: DIP Import blocks HMI

  27/08/2013: Herve Milcent
  DIP-120 - DIP and fwDipImport, accept "single" keyword in the import file and use "single" in the export instead of "simple"
  
  27/08/2013: Herve Milcent
  ENS-8248
  
@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@author
	Herve Milcent
*/

//@{
#uses "fwDIP/fwDIPImport.ctl"

//@}

//@{
//------------------------------------------------------------------------------------------------------------------------
/** returns the file name to be use for log file

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@return					the file name is returned
*/
string fwDIPExport_makeLogFileName()
{
  return getPath(LOG_REL_PATH) + "/fwDIPExport" + formatTime("_%Y_%m_%d_%H_%M_%S", getCurrentTime()) + ".txt";
}
//------------------------------------------------------------------------------------------------------------------------
/** Save the DIP config in a file

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param sDIPConfig		input, the DIP config DP name
@param sFile			input, the file to dump the DIP config into
@param bUseAlias			input, true=use the alias/false=use DP name
@param exceptionInfo	details of any exceptions are returned here
@param sLogFile			input, the log file name
*/
fwDIPExport_saveDIPConfigInFile(string sDIPConfig, string sFile, bool bUseAlias, dyn_string &exceptionInfo, string sLogFile)
{
  file fFile;
  dyn_dyn_string ddsCurrentPublicationInfo, ddsCurrentSubscriptionInfo;
  string sPublisherName, sQueryTimeOut;
  int iPos, iManagerNumber = -1;
  dyn_int diNumber;
  dyn_string dsConfigFileDIPConfig;
  int i, len;
  mapping mDIPConfig, mDIPPublication, mDIPSubscription;
  dyn_dyn_string ddsTemp;
  string sTemp, sTempDP;
  string sDns, sParameters;
  int iElapsedTime, iStartTime=(int) getCurrentTime();
  
  // if the log file is given, open it and dump info/warning/error messages
  if(sLogFile != "")
  {
    fFile = fopen(sLogFile, "a");
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " exporting DIP config: "+ sDIPConfig+" to file "+sFile+" starting\n", fFile);
  }
  // get the DIP config
  dpGet(sDIPConfig+".queryTimeout", sQueryTimeOut, sDIPConfig+".publishName", sPublisherName);
  // get all the publications and subscriptions
  fwDIP_getAllPublications(sDIPConfig, ddsCurrentPublicationInfo, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") +" "+exceptionInfo+"\n", fFile);
  fwDIP_getAllSubscriptions(sDIPConfig, ddsCurrentSubscriptionInfo, exceptionInfo);
  if(dynlen(exceptionInfo) > 0)
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") +" "+exceptionInfo+"\n", fFile);
  // get all the DIP config configured in the config file in order to retrieve the DIP API manager number
  _fwDIP_getAllDIPConfigFromConfigFile(diNumber, dsConfigFileDIPConfig);
  iPos = dynContains(dsConfigFileDIPConfig, sDIPConfig);
  if(iPos > 0)
  {
    // keep the DIP API manager config in a mapping
    iManagerNumber = diNumber[iPos];
    mDIPConfig["name"] = sDIPConfig;
    mDIPConfig["publisherName"] = sPublisherName;
    mDIPConfig["queryTimeout"] = sQueryTimeOut;
    mDIPConfig["managerNumber"] = (string)iManagerNumber;
    _fwDIPExport_getManagerDNSParameters(API_MAN, "WCCOAdip", iManagerNumber, sDns, sParameters);
    mDIPConfig["dns"] = sDns;
    mDIPConfig["parameters"] = sParameters;
    // get the publication config and keep it
    /**
      the publication are kept in a mapping variable:
      - key = DIP publication name
      - value = list of DPE and its parameter associated to the DIP publication name
      
      One can have more than one DPE per publication.
    **/
    if(dynlen(ddsCurrentPublicationInfo) >= fwDIP_OBJECT_UPDATE_RATES)
    {
      len = dynlen(ddsCurrentPublicationInfo[fwDIP_OBJECT_UPDATE_RATES]);
      iStartTime=(int) getCurrentTime();
      _fwDIP_setProgressBar(0, len);
      for(i=1;i<=len;i++)
      {
        _fwDIP_setProgressBar(i);
        iElapsedTime = (int) (getCurrentTime()-iStartTime);
        _fwDIP_setRemainingTime(iElapsedTime, i, len, true);
        if(mappingHasKey(mDIPPublication, ddsCurrentPublicationInfo[fwDIP_OBJECT_ITEM][i]))
          ddsTemp = mDIPPublication[ddsCurrentPublicationInfo[fwDIP_OBJECT_ITEM][i]];
        else
          dynClear(ddsTemp);
        dynAppend(ddsTemp, makeDynString(FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                         FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                         FWDIPIMPORT_CONST_BUFFER_DEFAULT, FWDIPIMPORT_CONST_OVERWRITE_DEFAULT, 
                                         FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                         FWDIPIMPORT_CONST_VARIABLE_NOT_SET));
        sTemp = dpSubStr(ddsCurrentPublicationInfo[fwDIP_OBJECT_DPES][i], DPSUB_DP_EL);
        sTempDP = dpSubStr(ddsCurrentPublicationInfo[fwDIP_OBJECT_DPES][i], DPSUB_DP);
        strreplace(sTemp, sTempDP, "");
        if(bUseAlias)
        {
          ddsTemp[dynlen(ddsTemp)][2] = dpGetAlias(sTempDP+"."); // alias 
          if(ddsTemp[dynlen(ddsTemp)][2] == "")
            ddsTemp[dynlen(ddsTemp)][1] = sTempDP; // DP if no alias
          ddsTemp[dynlen(ddsTemp)][3] = sTemp; // DPE
        }
        else
        {
          ddsTemp[dynlen(ddsTemp)][1] = sTempDP; // DP
          ddsTemp[dynlen(ddsTemp)][3] = sTemp; // DPE
        }
        ddsTemp[dynlen(ddsTemp)][4] = ddsCurrentPublicationInfo[fwDIP_OBJECT_TAGS][i]; // tags
        ddsTemp[dynlen(ddsTemp)][5] = ddsCurrentPublicationInfo[fwDIP_OBJECT_UPDATE_RATES][i]; //buffer
        ddsTemp[dynlen(ddsTemp)][6] = true; //overwrite
        
        mDIPPublication[ddsCurrentPublicationInfo[fwDIP_OBJECT_ITEM][i]] = ddsTemp;
      }
      _fwDIP_setProgressBar(0);
      _fwDIP_setRemainingTime(iElapsedTime, i, len, false);
    }
    
    // get the subscription config and keep it
    /**
      the subscription are kept in a mapping variable:
      - key = DIP subscription name
      - value = list of DPE and its parameter associated to the DIP subscription name
      
      One can have more than one DPE per subscription, however in the current implementation, one has only one DPE per subscription,
      because the complex subscription are not supported (ENS-8248).
    **/
    if(dynlen(ddsCurrentSubscriptionInfo) >= fwDIP_OBJECT_TAGS)
    {
      len = dynlen(ddsCurrentSubscriptionInfo[fwDIP_OBJECT_TAGS]);
      iStartTime=(int) getCurrentTime();
      _fwDIP_setProgressBar(0, len);
      for(i=1;i<=len;i++)
      {
        _fwDIP_setProgressBar(i);
        iElapsedTime = (int) (getCurrentTime()-iStartTime);
        _fwDIP_setRemainingTime(iElapsedTime, i, len, true);
        if(mappingHasKey(mDIPSubscription, ddsCurrentSubscriptionInfo[fwDIP_OBJECT_ITEM][i]))
          ddsTemp = mDIPSubscription[ddsCurrentSubscriptionInfo[fwDIP_OBJECT_ITEM][i]];
        else
          dynClear(ddsTemp);
        dynAppend(ddsTemp, makeDynString(FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                         FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                         FWDIPIMPORT_CONST_BUFFER_DEFAULT, FWDIPIMPORT_CONST_OVERWRITE_DEFAULT, 
                                         FWDIPIMPORT_CONST_VARIABLE_NOT_SET, FWDIPIMPORT_CONST_VARIABLE_NOT_SET, 
                                         FWDIPIMPORT_CONST_VARIABLE_NOT_SET));
        sTemp = dpSubStr(ddsCurrentSubscriptionInfo[fwDIP_OBJECT_DPES][i], DPSUB_DP_EL);
        sTempDP = dpSubStr(ddsCurrentSubscriptionInfo[fwDIP_OBJECT_DPES][i], DPSUB_DP);
        strreplace(sTemp, sTempDP, "");
        if(bUseAlias)
        {
          ddsTemp[dynlen(ddsTemp)][2] = dpGetAlias(sTempDP+"."); // alias 
          if(ddsTemp[dynlen(ddsTemp)][2] == "")
            ddsTemp[dynlen(ddsTemp)][1] = sTempDP; // DP if no alias
          ddsTemp[dynlen(ddsTemp)][3] = sTemp; // DPE
        }
        else
        {
          ddsTemp[dynlen(ddsTemp)][1] = sTempDP; // DP
          ddsTemp[dynlen(ddsTemp)][3] = sTemp; // DPE
        }
        ddsTemp[dynlen(ddsTemp)][4] = ddsCurrentSubscriptionInfo[fwDIP_OBJECT_TAGS][i]; // tags
        ddsTemp[dynlen(ddsTemp)][6] = true; //overwrite
        ddsTemp[dynlen(ddsTemp)][7] = dpTypeName(sTempDP); // type
        ddsTemp[dynlen(ddsTemp)][8] = dpGetDescription(ddsCurrentSubscriptionInfo[fwDIP_OBJECT_DPES][i]); // description
        ddsTemp[dynlen(ddsTemp)][9] = dpGetUnit(ddsCurrentSubscriptionInfo[fwDIP_OBJECT_DPES][i]); // unit
        ddsTemp[dynlen(ddsTemp)][10] = dpElementType(sTempDP+sTemp);
        
        mDIPSubscription[ddsCurrentSubscriptionInfo[fwDIP_OBJECT_ITEM][i]] = ddsTemp;
      }
      _fwDIP_setProgressBar(0);
      _fwDIP_setRemainingTime(iElapsedTime, i, len, false);
    }
    // save the DIP configuration in the file
    fwDIPExport_saveInXML(fFile, sFile, mDIPConfig, mDIPPublication, mDIPSubscription, exceptionInfo);
//    DebugN(mDIPConfig, ddsCurrentPublicationInfo, mDIPPublication, ddsCurrentSubscriptionInfo, mDIPSubscription);
  }
  else
  {
    if(sLogFile != "")
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR "+sDIPConfig+" not in config file\n", fFile);
    fwException_raise(
        exceptionInfo,
        "ERROR",
        "ERROR "+sDIPConfig+" not in config file",
        ""
    );
  }
  if(sLogFile != "")
    _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " exporting DIP config: "+ sDIPConfig+" to file "+sFile+" done\n", fFile);
  fclose(fFile, exceptionInfo);
}
//------------------------------------------------------------------------------------------------------------------------
/** Save the DIP config in a XML file

@par Constraints
	None

@par Usage
	Public

@par PVSS managers
	VISION, CTRL

@param fFile		input, the log file name id
@param sXMLFile			input, the file to dump the DIP config into
@param mDIPConfig			input, the DIPConfig data
@param mDIPPublication			input, the list of publication
@param mDIPSubscription			input, the list of subscription
@param exceptionInfo	details of any exceptions are returned here
*/
fwDIPExport_saveInXML(file fFile, string sXMLFile, mapping mDIPConfig, mapping mDIPPublication, mapping mDIPSubscription, dyn_string &exceptionInfo)
{
  int iDocNum, iNodeId, iRes, iParentNodeId, iTopNodeId, iPubNodeId, iDPEId, iSubNodeId;
  int i, len, iDPE, lenDPE;
  string sTemp, sXSDTemp;
  dyn_dyn_string ddsTemp;
  int iElapsedTime, iStartTime=(int) getCurrentTime();
  
  iDocNum = xmlNewDocument();
  if(iDocNum >= 0)
  {
    // processing instruction
    iNodeId = xmlAppendChild(iDocNum, -1, XML_PROCESSING_INSTRUCTION_NODE, "xml version=\"1.0\" encoding=\"ascii\"");
    // comment
    iNodeId = xmlAppendChild(iDocNum, -1, XML_COMMENT_NODE, formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d")+" Export DIP config");
    // DIP Config and all attributes
    iTopNodeId = xmlAppendChild(iDocNum, -1, XML_ELEMENT_NODE, "configuration");
    len = mappinglen(mDIPConfig);
    for(i=1;i<=len;i++)
      xmlSetElementAttribute(iDocNum, iTopNodeId, mappingGetKey(mDIPConfig, i), mappingGetValue(mDIPConfig, i));
    
    // all publications
    iNodeId = xmlAppendChild(iDocNum, iTopNodeId, XML_COMMENT_NODE, "DIP publication(s)");
    len=mappinglen(mDIPPublication);
    iStartTime=(int) getCurrentTime();
    _fwDIP_setProgressBar(0, len);
    for(i=1;i<=len;i++)
    {
      _fwDIP_setProgressBar(i);
      iElapsedTime = (int) (getCurrentTime()-iStartTime);
      _fwDIP_setRemainingTime(iElapsedTime, i, len, true);
      iPubNodeId = xmlAppendChild(iDocNum, iTopNodeId, XML_ELEMENT_NODE, "publication");
      xmlSetElementAttribute(iDocNum, iPubNodeId, "name", mappingGetKey(mDIPPublication, i));
      ddsTemp = mappingGetValue(mDIPPublication, i);
      lenDPE = dynlen(ddsTemp);
      if(lenDPE > 0)
      {
        if(lenDPE > 1)
           sTemp = "multiple";
        else
          sTemp = "single";
        xmlSetElementAttribute(iDocNum, iPubNodeId, "type", sTemp);
        if(fFile != 0)
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO publication: "+mappingGetKey(mDIPPublication, i)+", type: "+sTemp+", "+lenDPE +" DPE \n", fFile);
        for(iDPE=1;iDPE<=lenDPE;iDPE++)
        {
          iDPEId = xmlAppendChild(iDocNum, iPubNodeId, XML_ELEMENT_NODE, "dpe");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "name");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][1]);
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "alias");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][2]);
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "element");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][3]);
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "tag");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][4]);
          iDPEId = xmlAppendChild(iDocNum, iPubNodeId, XML_ELEMENT_NODE, "buffer");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][5]);
          iDPEId = xmlAppendChild(iDocNum, iPubNodeId, XML_ELEMENT_NODE, "overwrite");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][6]);
          iElapsedTime = (int) (getCurrentTime()-iStartTime);
          _fwDIP_setRemainingTime(iElapsedTime, i, len, true);
          if(fFile != 0)
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO DPE: "+ddsTemp[iDPE][1]+", alias: "+ddsTemp[iDPE][2]+", element: "+ddsTemp[iDPE][3]
                  +", tag: "+ddsTemp[iDPE][4]+", buffer: "+ddsTemp[iDPE][5]+", overwrite: "+ddsTemp[iDPE][6]+"\n", fFile);
        }
      }
      else
      {
        if(fFile != 0)
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING publication: "+mDIPPublication["name"]+" 0 DPE size, skipping publication\n", fFile);
        fwException_raise(
            exceptionInfo,
            "WARNING",
            "WARNING publication: "+mDIPPublication["name"]+" 0 DPE size, skipping publication",
            ""
        );
      }
    }
    _fwDIP_setProgressBar(0);
    _fwDIP_setRemainingTime(iElapsedTime, i, len, false);
    
    // all subscriptions
    iNodeId = xmlAppendChild(iDocNum, iTopNodeId, XML_COMMENT_NODE, "DIP subscription(s)");
    len=mappinglen(mDIPSubscription);
    iStartTime=(int) getCurrentTime();
    _fwDIP_setProgressBar(0, len);
    for(i=1;i<=len;i++)
    {
      _fwDIP_setProgressBar(i);
      iElapsedTime = (int) (getCurrentTime()-iStartTime);
      _fwDIP_setRemainingTime(iElapsedTime, i, len, true);
      iSubNodeId = xmlAppendChild(iDocNum, iTopNodeId, XML_ELEMENT_NODE, "subscription");
      xmlSetElementAttribute(iDocNum, iSubNodeId, "name", mappingGetKey(mDIPSubscription, i));
      ddsTemp = mappingGetValue(mDIPSubscription, i);
      lenDPE = dynlen(ddsTemp);
      if(lenDPE > 0)
      {
        if(lenDPE > 1)
           sTemp = "multiple";
        else
          sTemp = "single";
        xmlSetElementAttribute(iDocNum, iSubNodeId, "type", sTemp);
        sXSDTemp = _fwDIPExport_convertType(ddsTemp[1][10]);
        xmlSetElementAttribute(iDocNum, iSubNodeId, "xsdType", sXSDTemp);
        if(fFile != 0)
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO subscription: "+mappingGetKey(mDIPSubscription, i)+", type: "+sTemp+", xsdType: "+sXSDTemp+", "+lenDPE +" DPE \n", fFile);
        for(iDPE=1;iDPE<=lenDPE;iDPE++)
        {
          iDPEId = xmlAppendChild(iDocNum, iSubNodeId, XML_ELEMENT_NODE, "dpe");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "name");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][1]);
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "alias");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][2]);
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "element");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][3]);
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_ELEMENT_NODE, "tag");
          iNodeId = xmlAppendChild(iDocNum, iNodeId, XML_TEXT_NODE, ddsTemp[iDPE][4]);
          iDPEId = xmlAppendChild(iDocNum, iSubNodeId, XML_ELEMENT_NODE, "buffer");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][5]);
          iDPEId = xmlAppendChild(iDocNum, iSubNodeId, XML_ELEMENT_NODE, "overwrite");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][6]);
          iDPEId = xmlAppendChild(iDocNum, iSubNodeId, XML_ELEMENT_NODE, "type");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][7]);
          iDPEId = xmlAppendChild(iDocNum, iSubNodeId, XML_ELEMENT_NODE, "description");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][8]);
          iDPEId = xmlAppendChild(iDocNum, iSubNodeId, XML_ELEMENT_NODE, "unit");
          iNodeId = xmlAppendChild(iDocNum, iDPEId, XML_TEXT_NODE, ddsTemp[iDPE][9]);
          iElapsedTime = (int) (getCurrentTime()-iStartTime);
          _fwDIP_setRemainingTime(iElapsedTime, i, len, true);
          if(fFile != 0)
            _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO DPE: "+ddsTemp[iDPE][1]+", alias: "+ddsTemp[iDPE][2]+", element: "+ddsTemp[iDPE][3]
                  +", tag: "+ddsTemp[iDPE][4]+", buffer: "+ddsTemp[iDPE][5]+", overwrite: "+ddsTemp[iDPE][6]+"\n", fFile);
        }
      }
      else
      {
        if(fFile != 0)
          _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " WARNING subscription: "+mDIPSubscription["name"]+" 0 DPE size, skipping subscription\n", fFile);
        fwException_raise(
            exceptionInfo,
            "WARNING",
            "WARNING publication: "+mDIPSubscription["name"]+" 0 DPE size, skipping subscription",
            ""
        );
      }
    }
    _fwDIP_setProgressBar(0);
    _fwDIP_setRemainingTime(iElapsedTime, i, len, false);
    
    // save it to the file
    iRes = xmlDocumentToFile(iDocNum, sXMLFile);
    xmlCloseDocument(iDocNum);
    if(iRes >= 0)
    {
      if(fFile != 0)
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " INFO writing file: "+sXMLFile+" sucessfull\n", fFile);
    }
    else
    {
      if(fFile != 0)
        _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR writing file: "+sXMLFile+"\n", fFile);
      fwException_raise(
          exceptionInfo,
          "ERROR",
          "ERROR writing file: "+sXMLFile,
          ""
      );
    }
  }
  else
  {
    if(fFile != 0)
      _fwDIP_fputs(formatTime("%Y.%m.%d %H.%M.%S:",getCurrentTime(),"%03d") + " ERROR cannot create a XML doc\n", fFile);
    fwException_raise(
        exceptionInfo,
        "ERROR",
        "ERROR cannot create a XML doc",
        ""
    );
  }
}
//------------------------------------------------------------------------------------------------------------------------
/** Convert the DPE type to xml DIP type

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param iDPEType		input, the type of DPE
@return					the converted type
*/
string _fwDIPExport_convertType(int iDPEType)
{
  string sReturn="UNKNOWN";
  switch(iDPEType)
  {
    case DPEL_BOOL:
      sReturn = "xsd:bool";
      break;
    case DPEL_FLOAT:
      sReturn = "xsd:float";
      break;
    case DPEL_STRING:
      sReturn = "xsd:string";
      break;
    case DPEL_INT:
      sReturn = "xsd:int";
      break;
  }
  return sReturn;
}
//------------------------------------------------------------------------------------------------------------------------
/** Get the manager DNS and parameters

@par Constraints
	None

@par Usage
	Internal

@par PVSS managers
	VISION, CTRL

@param iManagerType		input, the type of manager: DRIVER_MAN, API_MAN
@param sManager		input, the manager name
@param iManagerNumber		input, the manager number
@param sDns		output, the dns used
@param sParameters		output, the extra parameters
*/
_fwDIPExport_getManagerDNSParameters(int iManagerType, string sManager, int iManagerNumber, string &sDns, string &sParameters)
{
  int iResult = -1;
  dyn_mixed properties;
  string sHost, sIP, sSystem, user, pwd, sCommandLine;
  int iPort;
  int iPos, iPos2;
  string sTemp;
  
  sSystem=getSystemName();
	
  if(isFunctionDefined("unGenericDpFunctions_getHostName"))
  {
    unGenericDpFunctions_getHostName(sSystem, sHost, sIP);	
    unGenericDpFunctions_getpmonPort(sSystem, iPort);
  }
		
  if (sHost=="" || iPort==0)
  {
    sHost=getHostname();
    iPort=pmonPort();
  }
  
  if(fwInstallation_getPmonInfo(user, pwd) != 0)
  {
    return;
  }
  
  iResult = fwInstallationManager_getProperties(sManager, "-num "+iManagerNumber, properties, sHost, iPort, user, pwd);
  
  if(iResult == 0)
  {
    if(properties[FW_INSTALLATION_MANAGER_PMON_IDX] == -1)
      iResult = -1;
    else
    { // take the first one
      sCommandLine = properties[FW_INSTALLATION_MANAGER_OPTIONS];
      strreplace(sCommandLine, "-num "+iManagerNumber, "");
      iPos = strpos(sCommandLine, "-dns");
      sDns = "";
      sParameters = "";
      if(iPos>0)
      {
        iPos2 = strpos(sCommandLine, "-", iPos+1);
        if(iPos2 == -1)
          iPos2= strlen(sCommandLine);
        iPos2 = iPos2 - (iPos+1);
        sTemp = substr(sCommandLine, iPos, iPos2+1);
        strreplace(sCommandLine, sTemp, "");
        strreplace(sTemp, "-dns", "");
        sDns = strltrim(strrtrim(sTemp));
//        DebugN(sCommandLine, iPos, strpos(sCommandLine, "-", iPos+1), iPos2, sTemp);
      }
      iPos = strpos(sCommandLine, "-");
      if(iPos>0)
      {
        iPos2 = strpos(sCommandLine, "-", iPos+1);
        if(iPos2 == -1)
          iPos2= strlen(sCommandLine);
        iPos2 = iPos2 - (iPos+1);
        sTemp = substr(sCommandLine, iPos, iPos2+1);
        sParameters = strltrim(strrtrim(sTemp));
      }
    }
  }
//  DebugN(iResult, sManager, sCommandLine, properties);
}

//------------------------------------------------------------------------------------------------------------------------
//@}
