#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwGeneral/fwGeneral.ctl"

const string MYDPTNAME =  "ELMBType";
const string MYDPNAME = "myDpName";
dyn_string dpEexpectedNames;
dyn_string dpETexpectedCodes;

/**
 * 
 */
void test_FwGeneral_getDpelementssetupSuite() {
	// TODO: implement test
}

/**
 * 
 */
void test_setup(){
	
	dyn_dyn_string xxdpes;
	dyn_dyn_int xxdpei;
	int replyCode;

// Create the data type
DebugTN("Entering test_setup()");
xxdpes[1] = makeDynString (MYDPTNAME,"");
xxdpes[2] = makeDynString ("","id");
xxdpes[3] = makeDynString ("","model");
xxdpes[4] = makeDynString ("","value");

xxdpei[1] = makeDynInt (DPEL_STRUCT,0);
xxdpei[2] = makeDynInt (0,DPEL_STRING);
xxdpei[3] = makeDynInt (0,DPEL_STRING);
xxdpei[4] = makeDynInt (0,DPEL_BOOL);
DebugTN("About to dpTypeCreate()");
DebugTN("xxdpes = ",xxdpes);
DebugTN("xxdpei = ",xxdpei);

// Create the datapoint type
replyCode = dpTypeCreate(xxdpes,xxdpei);
DebugN ("ELMBType DPT created, replyCode = ",replyCode);

//create a datapoint
replyCode = dpCreate (MYDPNAME, MYDPTNAME);
DebugTN("dpCreate returned repluyCode = ",replyCode);

//define the arrays of what we expect
dpEexpectedNames=  makeDynString (".",".id",".model",".value") ;

//dpETexpectedNames=  makeDynString ("BOOL","dyn_string","dyn_string");
dpETexpectedCodes=  makeDynString ((string)DPEL_STRUCT,(string)DPEL_STRING, (string)DPEL_STRING, (string) DPEL_BOOL);
DebugTN("DPEL_BOOL = ",DPEL_BOOL);
DebugTN("As a string, (string)DPEL_BOOL = ",(string) DPEL_BOOL);
DebugTN("dpETexpectedCodes = ",dpETexpectedCodes);
}

/** test for every cases.
 * 
 */

void test_getDpElementsDpOnly() {
	dyn_string dpElements;
	dyn_string dpElementTypes;
	dyn_string exceptionInfo;
	fwGeneral_getDpElements (MYDPNAME,"",dpElements,dpElementTypes,exceptionInfo);
	assertEqual(dynlen(exceptionInfo) , 0);
	assertEqual (dpEexpectedNames,dpElements);
	assertEqual (dpETexpectedCodes,dpElementTypes);
	}
void test_getDpElementsDpTOnly() {
	dyn_string dpElements;
	dyn_string dpElementTypes;
	dyn_string exceptionInfo;	
	fwGeneral_getDpElements ("",(string) MYDPTNAME,dpElements,dpElementTypes,exceptionInfo);
	assertEqual(dynlen(exceptionInfo) , 0);
	assertEqual (dpEexpectedNames,dpElements);
	assertEqual (dpETexpectedCodes,dpElementTypes);
	}
void test_getDpElementsBoth() {
	dyn_string dpElements;
	dyn_string dpElementTypes;
	dyn_string exceptionInfo;	
	fwGeneral_getDpElements (MYDPNAME,(string) MYDPTNAME,dpElements,dpElementTypes,exceptionInfo);
	assertEqual(0,dynlen(exceptionInfo));
	assertEqual (dpEexpectedNames,dpElements);
	assertEqual (dpETexpectedCodes,dpElementTypes);
	}
void test_getDpElementsNeither() {
	dyn_string dpElements; 
	dyn_string dpElementTypes;
	dyn_string exceptionInfo;	
	fwGeneral_getDpElements ("","",dpElements,dpElementTypes,exceptionInfo);
	assertEqual(3,dynlen(exceptionInfo));
	DebugTN("exceptionInfo = ",exceptionInfo);
	//assertEqual (dpEexpectedNames,dpElements);
	//assertEqual (dpETexpectedCodes,dpElementTypes);
}

/**
 * 
 */
void test_teardown() {
	dpDelete (MYDPNAME);
	dpTypeDelete (MYDPTNAME);
	DebugTN("teardown complete");
}

/**
 * 
 */
void test_teardownSuite() {
	// TODO: implement test
}

