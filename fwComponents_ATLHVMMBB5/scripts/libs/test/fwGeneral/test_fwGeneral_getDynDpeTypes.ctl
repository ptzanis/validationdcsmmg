#uses "fwGeneral/fwGeneral.ctl"
#uses "fwUnitTestComponentAsserts.ctl"
dyn_string  dynStringExpected;

/**
 * 
 */
void test_FwGeneral_getDynDpeTypes() {
	// TODO: implement test
}



/**
 * 
 */
void test_setupSuite() {
	// TODO: implement test
}

/**
 * 
 */

void test_setup() {
	dynStringExpected = makeDynString (	(string) 	DPEL_DYN_BIT32,
													DPEL_DYN_BLOB,
													DPEL_DYN_BOOL,		
													DPEL_DYN_CHAR,			
													DPEL_DYN_DPID,			
													DPEL_DYN_FLOAT,			
													DPEL_DYN_INT,			
													DPEL_DYN_LANGSTRING,	
													DPEL_DYN_STRING,		
													DPEL_DYN_TIME,			
													DPEL_DYN_UINT);
}

/**
 * 
 */
void test_fwGeneral_getDynDpeTypes_() {
	dyn_string dynString;
	dyn_string expectinfo;
	fwGeneral_getDynDpeTypes(dynString, expectinfo);
	assertEqual (dynStringExpected,dynString);
}

/**
 * 
 */
void test_teardown() {
	// TODO: implement test
}

/**
 * 
 */
void test_teardownSuite() {
	// TODO: implement test
}

