#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwGeneral/fwGeneral.ctl"


void test_fwGeneral_removeFileExtension_emptyPath()
{
  string sFilePath = "";
  string sExpectedOutput= "";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

void test_fwGeneral_removeFileExtension_fileNameWithExtension()
{
  string sFilePath = "test.txt";
  string sExpectedOutput= "test";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

void test_fwGeneral_removeFileExtension_fileNameWithoutExtension()
{
  string sFilePath = "dummy";
  string sExpectedOutput= "dummy";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

void test_fwGeneral_removeFileExtension_filePathWithExtension()
{
  string sFilePath = "dummy/file.a";
  string sExpectedOutput= "dummy/file";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

void test_fwGeneral_removeFileExtension_filePathWithoutExtension()
{
  string sFilePath = "dummy/file";
  string sExpectedOutput = "dummy/file";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

void test_fwGeneral_removeFileExtension_filePathWithExtensionAndDots()
{
  string sFilePath = "dummy.d/file.abc.def";
  string sExpectedOutput = "dummy.d/file.abc";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

void test_fwGeneral_removeFileExtension_filePathWithoutExtensionButDots()
{
  string sFilePath = "dummy.d/file";
  string sExpectedOutput = "dummy.d/file";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}

/**void test_fwGeneral_removeFileExtension_unixHiddenFilePathWithoutExtension() // This fails currently
{
  string sFilePath = "dummy/.file";
  string sExpectedOutput = "dummy/.file";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}**/

void test_fwGeneral_removeFileExtension_windowsFilePathWithoutExtensionButDots()
{
  string sFilePath = "dummy.d\\file";
  string sExpectedOutput = "dummy.d\\file";
  assertEqual(sExpectedOutput, fwGeneral_removeFileExtension(sFilePath));
}
