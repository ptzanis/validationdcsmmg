#uses "fwInstallationManager.ctl"
#uses "fwUnitTestComponentAsserts.ctl"
#uses "fwConfigs/fwPeriphAddressBACnet.ctl"

// Constants
const string dummyPollGroup = "_dummyPollGroup"; // Name must have "_" prefix
const string dummyDp = "dummyDp"; // Our dummy dp that we will perform experiments on
const string dummyDpType = "ExampleDP_Int"; // Dummy dp type
const string pollGroupType = "_PollGroup";
anytype undef; // Hack to create undefined value
const int dummyDriverNum = 1; // This has to be unique, we assume test system will not reach this number


/**
* Sets up the test suite for BACnet.
* 
* @return void
*/
void test_fwPeriphAddressBACnet_setupSuite()
{
  addDummyPollGroup();
  addDummyDp();
  addDummyDriver();
}

/**
* Tears down the test suit for BACnet.
*
* @return void
*/
void test_fwPeriphAddressBACnet_teardownSuite()
{
  removeDummyPollGroup();
  removeDummyDp();
  removeDummyDriver();
}

/** Positive check on "check" function for BACnet 
 * 
 * @return value of type 'void'
 */
void test_fwPeriphAddressBACnet_checkPositive()
{
  dyn_anytype c = getGoodBACnetConfig(); // c - config
  
  dyn_string ex = makeDynString();
  _fwPeriphAddressBACnet_check(c, ex);
  assertEmpty(ex, "Positive test on _fwPeriphAddressBACnet_check() failed");
}

/** Negative tests on "check" function for BACnet 
 * 
 * @return value of type 'void'
 */
void test_fwPeriphAddressBACnet_checkNegative()
{
  dyn_anytype c = getGoodBACnetConfig(); // c - config
  dyn_anytype cBad = makeDynAnytype("foo", "bar");
  dyn_anytype cNotBACnet = corruptList(c, FW_PARAMETER_FIELD_COMMUNICATION);
  dyn_anytype cBadDatatype = corruptList(c, FW_PARAMETER_FIELD_DATATYPE, 100);
  dyn_anytype cBadReference = corruptList(c, FW_PARAMETER_FIELD_ADDRESS, "some.thing.to.check");
  dyn_anytype cBadPollGroup = corruptList(c, fwPeriphAddress_BACNET_POLL_GROUP);

  // List of bad configs + match for exception description (remember to update both)  
  dyn_anytype cList = makeDynAnytype(
    cBad,
    cNotBACnet,
    cBadDatatype,
    cBadReference,
    cBadPollGroup);
  dyn_string cExpectedEx = makeDynString(
    "bad size",
    "seems not to be a BACnet config",
    "not a valid _datatype",
    "reference address",
    "poll group");
  int cListSize = dynlen(cList);
  
  for(int i=1; i<=cListSize; i++)
  {
    dyn_string ex = makeDynString();
    anytype elem = cList[i];
    _fwPeriphAddressBACnet_check(elem, ex);

    // First check if there was an exception (it should)
    int exSize = dynlen(ex);
    assertTrue(exSize > 0, "Expected to fail, check case: " + cExpectedEx[i]);

    // Check if proper exception was thrown
    string exStr = ex[2]; // 2 - sorry, fwException doesn't have constants for getting the err message
    int pos = strpos(exStr, cExpectedEx[i]);
    string msg = "Invalid exception thrown, expecting to find: '" +
                 cExpectedEx[i] + "', got: '" + exStr + "'";
    assertTrue(pos >= 0, msg);
  }
}

/** Test getConfig functionality for BACnet. 
 */
void test_fwPeriphAddressBACnet_setGetConfig()
{
  dyn_anytype cSet = getGoodBACnetConfig();
  string dummyDpDot = dummyDp + "."; // Datapoint has to contain at least one dot
  
  dyn_string ex;
  _fwPeriphAddressBACnet_set(dummyDpDot, cSet, ex);
  string msg = "Failed to set _address config on " + dummyDpDot;
  assertEmpty(ex, msg);
  
  dyn_anytype cGet;
  bool isActive;
  dynClear(ex);
  _fwPeriphAddressBACnet_get(dummyDpDot, cGet, isActive, ex);
  msg = "Failed to get config for dp: " + dummyDpDot;
  assertEmpty(ex, msg);
  
  // Now check if what we got is what we expected
  int len = dynlen(cGet);
  bool bad = false;
  for(int i=1; i<=len; i++)
  {
    anytype expected = cSet[i];
    anytype actual = cGet[i];
    
    // Special case for poll group - remove potential system name
    if(i == fwPeriphAddress_BACNET_POLL_GROUP)
    {
      // Remove possible system
      actual = dpSubStr((string) actual, DPSUB_DP_EL_CONF_DET_ATT);
    }

    if(expected != actual)
    {
      sprintf(msg, "Config %d: expected: %s, got: %s", i, (string) expected, (string) actual);
      DebugTN(msg);
      bad = true;
    }
  }
  assertFalse(bad, "Got different config from _fwPeriphAddressBACnet_get() function than expected");
}


/** Helper functions
 * 
 */

/** Creates dummy poll group 
 */
int addDummyPollGroup()
{
  return dpCreate(dummyPollGroup, pollGroupType);
}

/** Removes dummy poll group 
 * 
 * @return value of type 'void'
 */
int removeDummyPollGroup()
{
  if(dpExists(dummyPollGroup))
  {
    return dpDelete(dummyPollGroup);
  }
  
  return 0;
}

int addDummyDp()
{
  return dpCreate(dummyDp, dummyDpType);
}

int removeDummyDp()
{
  if(dpExists(dummyDp))
  {
    return dpDelete(dummyDp);
  }
  
  return 0;
}

/** Add simulation driver for our experiments. */
void addDummyDriver()
{
  // TODO: getting a working simulation driver could be improved.

  // Note: for whatever reason if you try to add driver no 44 NOT
  //       using GUI interface - it will fail

  // Only add if it doesn't exists
  if(!fwInstallationManager_isDriverRunning(dummyDriverNum))
  {
    // Add dummy simulation driver
    int retVal = fwInstallationManager_appendDriver(
      "SIM", // Type of driver 
      "dummy title", // Title for the window (not used)
      "WCCILsim", // Driver type
      "manual", // Start mode, otherwise we might risk starting a UI dialog
      5, // Restart period
      1, // How many restarts allowed
      1, // How many restarts to perform
      "-num " + dummyDriverNum
    );
    
    if(retVal != 1)
    {
      DebugTN("Failed to add simulation driver no " + dummyDriverNum +
              ", ignoring... (return code: " + retVal + ")");
    }
  }
  else
  {
    DebugTN("Simulation driver no " + dummyDriverNum +
            " already exists, skipping addition");
  }
  
  DebugTN("Starting simulation driver no " + dummyDriverNum);
  fwInstallationManager_command(
    "START",
    "WCCILsim",
    "-num " + dummyDriverNum
    );
}

/** Remove simulation driver */
void removeDummyDriver()
{
  // No functionality to remove driver :(
}

/** Returns good config for BACnet.
 * 
 * @return value of type 'dyn_anytype' good config for BACnet
 */
dyn_anytype getGoodBACnetConfig()
{
  dyn_anytype c;
  c[fwPeriphAddress_BACNET_OBJECT_SIZE] = ""; // Force size on list (create it with a given size)
  
  c[FW_PARAMETER_FIELD_COMMUNICATION] = fwPeriphAddress_TYPE_BACNET;
  c[FW_PARAMETER_FIELD_DRIVER] = dummyDriverNum;
  c[FW_PARAMETER_FIELD_ADDRESS] = "WagoTest1.AnalogInput.0.Units";
  c[FW_PARAMETER_FIELD_MODE] = 4;
  c[FW_PARAMETER_FIELD_DATATYPE] = fwPeriphAddress_BACNET_TYPE_DEFAULT;
  c[FW_PARAMETER_FIELD_ACTIVE] = 0;
  c[FW_PARAMETER_FIELD_LOWLEVEL] = 0;
  c[fwPeriphAddress_BACNET_POLL_GROUP] = dummyPollGroup;

  return c;
}

/** Corrupt given list.
 * 
 * By default random element is corrupted.
 * Corruption = some string is asssigned. 
 * 
 * @param l	(dyn_anytype)	IN list to be corrupted
 * @param idx	(int)	IN  is optional index to be corrupted 
 * @param value	(anytype)	IN  is optional value to be assigned
 * @return value of type 'dyn_anytype' returns corrupted list, in case of problems, non-corrupted list
 */
dyn_anytype corruptList(dyn_anytype l, int idx=-1, anytype value=undef)
{
  int listSize = dynlen(l);
  // If idx < 0 just pick a random number
  if(idx < 0)
  {
    idx = randRange(1, listSize);
  }
  
  // Check if requested idx is not too high
  if(idx > listSize)
  {
    return l; // In this case - no corruption
  }
  
  if(getType(value) == ANYTYPE_VAR)
  {
    value = "DEADBEEFDEADBEEF"; // We assume this is an invalid value
  }
  l[idx] = value;

  return l;
}

/** Returns random range. 
 * 
 * @param low	(int)	IN low limit
 * @param high	(int)	IN high limit
 * @return value of type 'int' random value
 */
int randRange(int low, int high)
{
  const int MAXRAND = 32767;
  float r = ((float)rand()/(float)MAXRAND);
 
  return low + (int)((high-low)*r);
}


/** Merges two lists of type anytype.
 * 
 * For undefined values in mask src element is not changed.
 * 
 * @param src	(dyn_anytype)	IN source list
 * @param mask	(dyn_anytype)	IN mask list
 * @return value of type 'dyn_anytype' list with overriden values, src list if both lists do not match
 */
dyn_anytype mergeLists(dyn_anytype src, dyn_anytype mask)
{
  dyn_anytype retList = makeDynAnytype();
  int srcListSize = dynlen(src);
  int maskListSize = dynlen(mask);

  // In case of size mismatch, just return source list
  if(srcListSize != maskListSize)
  {
    return src;
  }
  
  for(int i=1; i<=srcListSize; i++)
  {
    anytype elem = retList[i];
    if(getType(mask[i]) != ANYTYPE_VAR)
    {
      retList[i] = mask[i];
    }
    else
    {
      retList[i] = mask[i];
    }
  }
  
  return retList;
}

