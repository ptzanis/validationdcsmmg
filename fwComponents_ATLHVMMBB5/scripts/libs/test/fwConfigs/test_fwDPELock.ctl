#uses "fwConfigs/fwDPELock.ctl"
#uses "fwUnitTestComponentAsserts.ctl"

const string test_dp = "fwDPELock_test_tryLock";
const string peer_comm_dp="fwDPELock_test_control";
const int    peerManNum=50;

global mapping originalExceptionInfoSettings;


void sendCommandToPeer(string cmd)
{
    delay(0,250);
    dpSetWait(peer_comm_dp+".",cmd);
    delay(0,250);
}

void startTesterPeer()
{
    string script="libs/test/fwConfigs/test_fwDPELock_Peer.ctl";
    int manNum=peerManNum;

    DebugTN("Starting peer "+script+" in CTRL manager "+manNum);


    // calm down the error messages when starting the man num
    if (!dpExists("_CtrlDebug_CTRL_"+manNum)) {
	dpCreate("_CtrlDebug_CTRL_"+manNum,"_CtrlDebug");
	delay(0,100);
    }

    // wait for previous one to terminate, if needed
    string s;
    for (int i=1;i<=10;i++) {
	dpGet(peer_comm_dp+".:_original.._value",s);
	if (s=="") break;
	DebugTN("Waiting for the previous instance of peer to shut down...");
	delay(1,0);
    }


    // reset the command thing
    dpSetWait(peer_comm_dp+".","START");
    delay(0,100);

    string ctrlMan,cmd;
    if (_UNIX) {
	ctrlMan=getPath(BIN_REL_PATH,"WCCOActrl");
	cmd=ctrlMan+" -proj "+PROJ+" -num "+manNum+" "+script+" &";

    } else {
	ctrlMan= getPath(BIN_REL_PATH,"WCCOActrl.exe");
	cmd="START "+ctrlMan+" -proj "+PROJ+" -num "+manNum+" "+script;
    }
    system(cmd);
    // now wait until it is connected and alive 
    bool timedOut=false;
    dyn_mixed retValues;
    dpWaitForValue(makeDynString(peer_comm_dp+".:_original.._value"), // we wait on this one
		   makeDynAnytype("READY"),          // awaited value
		   makeDynString(peer_comm_dp+".:_original.._value"), // returned DPE values
		   retValues,			    // values will come here
		   10,				    // time to wait
		   timedOut			    // will indicate the timeout
		  );
    if (timedOut) {
	DebugTN("Peer does not seem to be started (no response)");
	return;
    }
    DebugTN("Peer is running now");
}

void finishTesterPeer()
{
    dpSetWait(peer_comm_dp+".","STOP");
    bool timedOut;
    dyn_anytype retValues;
    dpWaitForValue(makeDynString(peer_comm_dp+".:_original.._value"), // we wait on this one
		   makeDynAnytype(""),          // awaited value
		   makeDynString(peer_comm_dp+".:_original.._value"), // returned DPE values
		   retValues,			    // values will come here
		   2,				    // time to wait
		   timedOut			    // will indicate the timeout
		  );
    if (timedOut) {
	DebugTN("Could not shut down TesterPeer!");
    }
}


void assertExceptionInfo(string errSeverity, string errString, dyn_string exceptionInfo)
{
    // until Paul fixes the unit-testing framework's assertError() we need this one:
    if (dynlen(exceptionInfo) <3) {
	assert(false,"No exceptionInfo while expecting to have one");
	return;
    }
    assertError(errSeverity,errString,exceptionInfo);
}



private string getErrorString(int errCode)
{
    // convert a integer error code to a 5-digit string
    // as needed by getCatStr()
    string errCodeAsString;
    sprintf(errCodeAsString,"%05d",errCode);
    string errCat="_errors";
    string errString=getCatStr(errCat,errCodeAsString);
    return errString;
}


void assertLocked(string dpe, string config="_original") 
{
    bool isLocked;
    string exceptionText;
    try {
	isLocked=fwDPELock_isLocked(dpe+":"+config);
    } catch {
	dyn_errClass exc=getLastException();
	exceptionText=getErrorText(exc);
    } 

    assertEqual("",exceptionText);
    assertTrue(isLocked, "DPE is not locked while it should: " + dpe + ":" + config);
}

void assertNotLocked(string dpe, string config="_original") 
{

    bool isLocked;
    string exceptionText;
    try {
	isLocked=fwDPELock_isLocked(dpe+":"+config);
    } catch {
	dyn_errClass exc=getLastException();
	exceptionText=getErrorText(exc);
    } 

    assertEqual("",exceptionText);
    assertFalse(isLocked, "DPE is locked while it shouldn't: " + dpe + ":" + config);

}


void test_fwDPELock_tryLock_setupSuite() 
{

    // suppress printout of exceptionInfos in the log,
    // still preserve what was the original setting 
    // so that we could restore it.
    if(!globalExists("g_fwExceptionPriorities")) _fwException_initialise();
    originalExceptionInfoSettings = g_fwExceptionPriorities;
    g_fwExceptionPriorities["ERROR"]=-1;

    if(dpExists(test_dp)) dpDelete(test_dp);
    if(dpExists(peer_comm_dp)) dpDelete(peer_comm_dp);

    dpCreate(test_dp, "ExampleDP_Int");
    dpCreate(peer_comm_dp, "ExampleDP_Text");

    dpSetWait(test_dp + ".:_general.._type", DPCONFIG_GENERAL);

    startTesterPeer();
}

void test_fwDPELock_tryLock_teardownSuite() 
{
    finishTesterPeer();

    if(dpExists(test_dp)) dpDelete(test_dp);
    if(dpExists(peer_comm_dp)) dpDelete(peer_comm_dp);

    g_fwExceptionPriorities=originalExceptionInfoSettings;

}


private string check_getLockConfigTest(string dpeWithConfig, string expectedExceptionText="")
{
    string lockConfig,exceptionText;
    try {
        lockConfig=_fwDPELock_getLockConfig(dpeWithConfig);
    } catch {
	dyn_errClass exc=getLastException();
	exceptionText=getErrorText(exc);
    } 

    assertEqual(expectedExceptionText,exceptionText);

    return lockConfig;

}

void test_fwDPELock_getLockConfig_emptyDpeConfig()
{
    string dpeWithConfig="";
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_DPE_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}

void test_fwDPELock_getLockConfig_DpNoExistSysDpe()
{
    string dpeWithConfig="SomeSystem:SomeDPE.";
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_DPE_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}

void test_fwDPELock_getLockConfig_DpNoExistSysDpeConfig()
{
    string dpeWithConfig="SomeSystem:SomeDPE._someConfig";
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_DPE_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}

void test_fwDPELock_getLockConfig_SysDpeBadConfig()
{
    string dpeWithConfig=getSystemName()+test_dp+".:_someConfig";
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_CONFIG_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}


void test_fwDPELock_getLockConfig_DpeBadConfig()
{
    string dpeWithConfig=test_dp+".:_someConfig";
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_CONFIG_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}

void test_fwDPELock_getLockConfig_SysDpeConfig()
{
    string dpeWithConfig=getSystemName()+test_dp+".:_original";
    string lockConfig=check_getLockConfigTest(dpeWithConfig);
    assertEqual(getSystemName()+test_dp+".:_lock._original",lockConfig);
}

void test_fwDPELock_getLockConfig_DpeConfig()
{
    string dpeWithConfig=test_dp+".:_original";
    string lockConfig=check_getLockConfigTest(dpeWithConfig);
    assertEqual(getSystemName()+test_dp+".:_lock._original",lockConfig);
}

void test_fwDPELock_getLockConfig_SysDpe()
{
    string dpeWithConfig=getSystemName()+test_dp+".";
    string lockConfig=check_getLockConfigTest(dpeWithConfig);
    assertEqual(getSystemName()+test_dp+".:_lock._original",lockConfig);
}

void test_fwDPELock_getLockConfig_Dpe()
{
    string dpeWithConfig=test_dp+".";
    string lockConfig=check_getLockConfigTest(dpeWithConfig);
    assertEqual(getSystemName()+test_dp+".:_lock._original",lockConfig);
}

void test_fwDPELock_getLockConfig_Dp()
{
    string dpeWithConfig=test_dp;
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_CONFIG_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}

void test_fwDPELock_getLockConfig_SysDp()
{
    string dpeWithConfig=getSystemName()+test_dp;
    check_getLockConfigTest(dpeWithConfig,getErrorString(fwDPELock_ERR_CONFIG_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig);
}


void test_fwDPELock_isLocked_NotLocked()
{
    assertNotLocked(getSystemName()+test_dp+".");
}

void test_fwDPELock_isLocked_NotLockedGeneral()
{
    assertNotLocked(getSystemName()+test_dp+".","_general");
}

void test_fwDPELock_isLocked_BadConfig()
{

    bool isLocked;
    string exceptionText;
    string dpeWithConfig=getSystemName()+test_dp+".:_SomeConfig";
    try {
	bool isLocked=fwDPELock_isLocked(dpeWithConfig);
    } catch {
	dyn_errClass exc=getLastException();
	exceptionText=getErrorText(exc);
    } 

    assertEqual(getErrorString(fwDPELock_ERR_CONFIG_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig,exceptionText);

}


void test_fwDPELock_getLocked_BadDPE() 
{

    dyn_string exceptionInfo, lockDetails;
    string dpeWithConfig="SomeInvalidDpe.";

    bool isLocked=fwDPELock_getLocked(dpeWithConfig, lockDetails, exceptionInfo);
    assertExceptionInfo("ERROR",
			getErrorString(fwDPELock_ERR_DPE_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig, 
			exceptionInfo);
}

void test_fwDPELock_getLocked_OK_defaultConfigUnlocked() 
{

    dyn_string exceptionInfo, lockDetails;
    string dpeWithConfig=getSystemName()+test_dp+".";

    bool isLocked=fwDPELock_getLocked(dpeWithConfig, lockDetails, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertFalse(isLocked, "Could not lock config");

}

void test_fwDPELock_getLocked_OK_generalConfigUnlocked()
{

    dyn_string exceptionInfo, lockDetails;
    string dpeWithConfig=getSystemName()+test_dp+".:_general";

    bool isLocked=fwDPELock_getLocked(dpeWithConfig, lockDetails, exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertFalse(isLocked, "Could not lock config _general");

}


void test_fwDPELock_tryLock_BadDPE() 
{

    dyn_string exceptionInfo, lockDetails;
    string dpeWithConfig="SomeInvalidDpe.";

    bool isLocked=fwDPELock_tryLock(dpeWithConfig, exceptionInfo);
    assertExceptionInfo("ERROR",
			getErrorString(fwDPELock_ERR_DPE_DOES_NOT_EXIST)+", getLockConfig fails, "+dpeWithConfig, 
			exceptionInfo);

}

void test_fwDPELock_tryLock_dpConfigBlank() 
{
    dyn_string exceptionInfo;
    bool locked = fwDPELock_tryLock(test_dp + ".", exceptionInfo);
DebugTN("tryLock returned",locked, exceptionInfo);
    assertTrue(locked, "Could not lock config");
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertLocked(test_dp + ".");

    sendCommandToPeer("UNLOCK");
}

void test_fwDPELock_tryLock_dpConfigSpecified() 
{
    dyn_string exceptionInfo;
    bool locked = fwDPELock_tryLock(test_dp + ".:_general", exceptionInfo);

    assertTrue(locked, "Could not lock config _general");
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertLocked(test_dp + ".", "_general");


    sendCommandToPeer("UNLOCK");
    sendCommandToPeer("UNLOCK_GENERAL");

}


void test_fwDPELock_tryLock_AlreadyLockedByOther() 
{
    sendCommandToPeer("LOCK");

    dyn_string exceptionInfo;
    bool locked = fwDPELock_tryLock(test_dp + ".", exceptionInfo);
    sendCommandToPeer("UNLOCK");

    assertFalse(locked, "Locking should have failed");

    assertExceptionInfo("ERROR",
			"Cannot lock: "+test_dp+"."+" already locked by CTRL -num "+peerManNum+" #1 @"+getHostname() ,
			exceptionInfo);

    sendCommandToPeer("UNLOCK");

}

void test_fwDPELock_unlock_NotLocked()
{
    sendCommandToPeer("UNLOCK");

    dyn_string exceptionInfo;
    fwDPELock_unlock(test_dp + ".", exceptionInfo);
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertNotLocked(test_dp + ".");

    sendCommandToPeer("UNLOCK");

}


void test_fwDPELock_unlock_Locked()
{
    dyn_string exceptionInfo;
    sendCommandToPeer("UNLOCK");
    bool locked = fwDPELock_tryLock(test_dp + ".", exceptionInfo);
    assertTrue(locked, "Could not lock config");

    fwDPELock_unlock(test_dp + ".", exceptionInfo);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertNotLocked(test_dp + ".");

    sendCommandToPeer("UNLOCK");

}


void test_fwDPELock_unlock_LockedByOther()
{
    sendCommandToPeer("LOCK");
    assertLocked(test_dp + ".");

    dyn_string exceptionInfo;
    fwDPELock_unlock(test_dp + ".", exceptionInfo);



    assertExceptionInfo("ERROR",
			"Could not unlock: "+test_dp+"."+" another manager is keeping the lock: CTRL -num "+peerManNum+" #1 @"+getHostname() , 
			exceptionInfo);


    sendCommandToPeer("UNLOCK");

}

void test_fwDPELock_unlock_LockedByOther_Force()
{
    sendCommandToPeer("LOCK");
    assertLocked(test_dp + ".");

    dyn_string exceptionInfo;
    fwDPELock_unlock(test_dp + ".", exceptionInfo,true);
    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertNotLocked(test_dp + ".");

    sendCommandToPeer("UNLOCK");

}

void test_fwConfigs_getConfigLock()
{
    sendCommandToPeer("LOCK");

    string dpe=test_dp+".";
    string config=fwConfigs_PVSS_ORIGINAL;
    bool isLocked;
    dyn_string lockDetails,exceptionInfo;

    fwConfigs_getConfigLock(dpe, config, isLocked, lockDetails, exceptionInfo);
    assertTrue(isLocked, "DPE is not locked while it should: " + dpe + ":" + config);

    assertEmpty(exceptionInfo, "Exceptions have been raised");
    assertLocked(test_dp + ".");

    sendCommandToPeer("UNLOCK");

}
