//
// Atlas user functions template
// adapt and save under scripts/libs/fwAtlasUser.ctl
//

//
// USER function to determine the PC location with respect to UPS power
// (US or USA side) to react on UPS on battery signal from DSS
//
string fwAtlas_getUpsSide()
{
	string system = getSystemName();
	strreplace(system, ":", "");
	if (system == "ATLCICSCS"
			|| system == "ATLCICUSAL1"
			|| system == "ATLCICUSAL2"
			|| system == "ATLCICSDX1") return "USA15";
	else if (system == "ATLCICUS15") return "US15";
	else error("fwAtlas_getUpsSide(): Unknown system name "+system);
	return "";
}
