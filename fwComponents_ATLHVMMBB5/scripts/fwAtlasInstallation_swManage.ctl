#uses "fwInstallation.ctl"

const int INSTALLATION_TIMEOUT = 900; // 15mins

main(string centralMode)
{
  DebugTN("Starting fwAtlasInstallation_swManage.ctl.");

  DebugTN("Set up DPs. Now setting FSM DNS node...");

  string dnsNode;
  string machineName = _WIN32 ? getenv("COMPUTERNAME") : getenv("HOSTNAME");
  if (strpos(getHostByName(machineName), "10.")<0) 
  {
    dnsNode = "pcaticstest01";
    dpSet("fwInstallationInfo.lastSourcePath", "/winccoa/fwComponents/");  
  }
  else 
  {
    dnsNode = "pcatlfsmdns1,pcatlfsmdns2";
    dpSet("fwInstallationInfo.lastSourcePath", "/det/dcs/fwComponents/");  
  }
  dpSet("ToDo.dim_dns_node", dnsNode, "ToDo.autoStart", 0);

  if (centralMode == "yes")
  {
    dpDelete("fwInstallation_pendingActions");
    fwInstallation_createPendingActionsDp();
    dyn_string componentDps = dpNames("*", "_FwInstallationComponents");
    for (int i=1;i<=dynlen(componentDps);++i) {
      dpDelete(componentDps[i]);
    }
  }  
  
  fwAtlas_copyPathOpcUaCetificates();
  
  fwInstallationDB_setUseDB(true);

  if (fwInstallationDB_connect()!=0) {
    error("fwAtlasInstallation_swManage.ctl: No DB connection. Abort.");
    return;
  }
  if (fwInstallationDB_setCentrallyManaged(1)!=0) {
    error("fwAtlasInstallation_swManage.ctl: Unable to set central management mode.");
  }
  else {
    information("fwAtlasInstallation_swManage.ctl: Central management mode set.");
  }
  
  fwInstallationDB_closeDBConnection();
  fwInstallationManager_setMode("WCCOActrl", "-f fwInstallationAgent.lst", "manual");
  information("fwAtlasInstallation_swManage.ctl: Starting DB agent...");  
  fwInstallationManager_restart("WCCOActrl", "-f fwInstallationAgent.lst");
  information("fwAtlasInstallation_swManage.ctl: DB agent started.");

  // start fwFsmSrvr which had been in manual before to avoid FSM start
  //
  information("fwAtlasInstallation_swManage.ctl: Starting fwFsmSrvr...");  
  fwInstallationManager_setMode("WCCOActrl", "fwFsmSrvr", "always");
  fwInstallationManager_restart("WCCOActrl", "fwFsmSrvr");
  information("fwAtlasInstallation_swManage.ctl: fwFsmSrvr started.");

  //
  // waiting for project components to get in sync by checking
  // 1: project component sync status
  // 2: pending postInstall files
  // 3: ongoing postInstall
  //
  delay(30);
  information("fwAtlasInstallation_swManage.ctl: waiting for components to be installed... stay tuned (timeout = "+INSTALLATION_TIMEOUT/60.+" minutes).");
  bool componentStatus = false;
  bool pendingPostInstalls = false;
  int timeout = INSTALLATION_TIMEOUT;
  while (!componentStatus && timeout) {
    dyn_int projectStatus;
    dpGet("fwInstallation_agentParametrization.db.projectStatus", projectStatus);
    dyn_string postInstallFiles = fwInstallation_getProjectPendingPostInstalls();
    bool postInstallRunning = false;
    fwInstallationDBAgent_isPostInstallRunning(postInstallRunning);
    DebugTN("fwAtlasInstallation_swManage.ctl: Waiting status\nprojectStatus(components)="+projectStatus[FW_INSTALLATION_DB_STATUS_COMPONENT_INFO]+"\npostInstallFiles:"+postInstallFiles+"\npostInstallRunning = "+postInstallRunning);
    if ( (projectStatus[FW_INSTALLATION_DB_STATUS_COMPONENT_INFO] == 1)
	 && (dynlen(postInstallFiles)==0)
	 && (postInstallRunning==false) ) {
      DebugTN("fwAtlasInstallation_swManage.ctl: all conditions seem fullfilled", (projectStatus[FW_INSTALLATION_DB_STATUS_COMPONENT_INFO] == 1), (dynlen(postInstallFiles)==0), (postInstallRunning==false));
      break;
    }
    --timeout;
    delay(1);
  }
  if (!timeout) {
    error("fwAtlasInstallation_swManage.ctl: component installation seems not to have finished after"+INSTALLATION_TIMEOUT/60.+" minutes, aborting.");
  }
  fwAtlasAddEntryIntoAsciiConfig();
  information("fwAtlasInstallation_swManage.ctl: component info seems in sync with database now.");
  return;
}

fwAtlasAddEntryIntoAsciiConfig()
{
  dyn_string names = dpNames("*","_AsciiConfig");
  for (int i = 1; i < dynlen(names); i++)
  {
    dyn_string  CNSPaths,CNSViews;
    dpGet(names[i]+".CNSPaths",CNSPaths,names[i]+".CNSViews",CNSViews);
    if (dynlen(CNSPaths) == 0) 
    {
      dynAppend(CNSPaths,"");
      dpSet(names[i]+".CNSPaths",CNSPaths);
    }
    if (dynlen(CNSViews) == 0) 
    {
      dynAppend(CNSViews,"");
      dpSet(names[i]+".CNSViews",CNSViews);
    }
  }
}
