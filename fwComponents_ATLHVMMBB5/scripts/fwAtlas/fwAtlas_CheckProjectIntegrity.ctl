/* *********************************************************************************
 * Script to Check Project Integrity
 * by: O. Gutzwiller
 * Managers: Ui. Ctrl.
 *
 * Reads in the config file the defined <Project Paths>+<Pvss Paths>, 
 * and searches in all <ProjectPath>/bin (all types),
 *                 all <ProjectPath>/scripts (.ctl, .lst, .ctc),
 *                 all <ProjectPath>/scripts/libs (.ctl, .lst, .ctc),
 *                 all <ProjectPath>/panels (.pnl)
 * for duplicated files (files with the same name). 
 * Then, generates a file with list of duplicated files and directory names in the repository.
 * Output file is used to display info on the Webmonitoring.
 * Ouput a file under /det/dcs/PVSSProjects/ATLAS_DCS_subdet/data
 * Script is ment to be executed once a day.  
 *
 * Is then copied by a cronjob under /WWW/data
 * Mods: 05/07/2013: adapted for SLC6/WinCC -- changes in proj path names, use environment variables where possible
 * *********************************************************************************/

#uses "fwAtlas.ctl"
#uses "email.ctl"

bool SEND_EMAILS = false;
string OnCallList = "olivier.gutzwiller@cern.ch;";  


main()
{
  string sysName = getSystemName(); 
  sysName = strsplit(sysName, ":")[1];
  string subdet = substr(sysName, 3, 3);
  string OutputFile;
  
  time tTimeNow = getCurrentTime(); 
  string sFormattedTime = formatTime("%c", tTimeNow); 
  int iReturn; 
  string old_date;   
  string date = strsplit(sFormattedTime, " ")[1];
  
  //Read template file
  string path = _WIN32 ? REPOSITORY_WIN : getenv("DCS_REPO");
  strreplace(path, "//", "/");
  if (substr(path, strlen(path) - 1, 1) == "/") path = substr(path, 0, strlen(path) - 1);
  string templateFile = path + "/ATLAS_DCS_"+subdet+"/data/fwAtlas_duplicatedFiles_AllowedDuplicatedFiles_"+subdet+".txt";  
  string template = "";
  file f = fopen(templateFile, "r");    
  fileToString(templateFile, template); fclose(f);
  dyn_string AllowedDuplicatedFiles  = strsplit(template, "\n");

  //Read config file
  bool Linux;
  string ConfigFileName = getPath(CONFIG_REL_PATH) + "/config";
  string section = "general"; 
  dyn_string dir; paCfgReadValueList(ConfigFileName, section, "pvss_path", dir); 
  dyn_string dir2; paCfgReadValueList(ConfigFileName, section, "proj_path", dir2);   
  dynAppend(dir, dir2);        
  dyn_string dirConfigFile = dir;
  for (int i=1; i <= dynlen(dir); ++i) {  
    if (strpos(dir[i], "fwComponents_ATLGCSDISPLAY_311") >=0) {
      dynRemove(dir, i); }
    else { //excluded directory
      for (int j=1; j <= dynlen(AllowedDuplicatedFiles); ++j) {
        if (strlen(dir[i])>0 && strlen(AllowedDuplicatedFiles[j])>0) {
          if ( (strpos(AllowedDuplicatedFiles[j], dir[i]) >0) || (strpos(AllowedDuplicatedFiles[j], dir[i]) ==0) ) { 
            dynRemove(dir, i); }      
        }
      }
    }   
  } 
      
  //OutputFile
  OutputFile = path + "/ATLAS_DCS_"+subdet+"/data/fwAtlas_duplicatedFiles_"+sysName+".txt";   
  
 

//  while(1) {
    dyn_string Messages, EmailMessage;
//    if (date != old_date) {  

      //BINARIES
      //Reads all files in /bin and in all subdir
      dyn_dyn_string BinFiles = readFiles(dir, "bin");
      //Compares if files have same name If yes, warning message + email sent to subdet responsible
      dyn_string BinDuplicated = compareFiles(BinFiles, dir, "bin", AllowedDuplicatedFiles);
    
      //SCRIPTS
      //Reads all .ctl,.lst,.ctc files in /scripts and in all subdir (except /libs)
      dyn_dyn_string ScriptsFiles = readFiles(dir, "scripts");
      //Compares if files have same name If yes, warning message + email sent to subdet responsible
      dyn_string ScriptsDuplicated = compareFiles(ScriptsFiles, dir, "scripts", AllowedDuplicatedFiles);

      //LIBS
      //Reads all .ctl,.lst,.ctc files in /libs and in all subdir
      dyn_dyn_string LibsFiles = readFiles(dir, "libs");      
      //Compares if files have same name. If yes, warning message + email sent to subdet responsible
      dyn_string LibsDuplicated = compareFiles(LibsFiles, dir, "libs", AllowedDuplicatedFiles);   

      //PANELS
      //Reads all .pnl files in /panels and in all subdir
      dyn_dyn_string PanelsFiles = readFiles(dir, "panels");
      //Compares if files have same name. If yes, warning message + email sent to subdet responsible
      dyn_string PanelsDuplicated = compareFiles(PanelsFiles, dir, "panels", AllowedDuplicatedFiles);    
      
      //Outputs results in a text file 
      //Lists duplicated files with their directory names      
      file f= fopen(OutputFile, "w");
      fputs("<header>\n" ,f);
      fputs("fwAtlas/fwAtlas_CheckProjectIntegrity.ctl - Duplicated files on "+sysName+" project\n" ,f);
      time tTimeNow = getCurrentTime(); 
      string sFormattedTime = formatTime("%d", tTimeNow)+"/"+formatTime("%m", tTimeNow)+"/"+formatTime("%Y", tTimeNow)+" "+formatTime("%H", tTimeNow)+":"+formatTime("%M", tTimeNow)+":"+formatTime("%S", tTimeNow)+" "; 
      fputs("Date: "+sFormattedTime+"\n" ,f);      
      fputs("</header>\n" ,f);
      fputs("<dir>\n" ,f);      
      for (int z=1; z <= dynlen(dirConfigFile); ++z) {
        fputs(dirConfigFile[z]+"\n" ,f); }
      fputs("</dir>\n" ,f);                              
      fputs("<bin>\n" ,f);      
      for (int z=1; z <= dynlen(BinDuplicated); ++z) {
        strreplace(BinDuplicated[z], ",", "\t");
        strreplace(BinDuplicated[z], ";", "\n");        
        fputs(BinDuplicated[z] ,f); }
      fputs("</bin>\n" ,f);                  
      fputs("<scripts>\n" ,f);      
      for (int z=1; z <= dynlen(ScriptsDuplicated); ++z) {
        strreplace(ScriptsDuplicated[z], ",", "\t");
        strreplace(ScriptsDuplicated[z], ";", "\n");        
        fputs(ScriptsDuplicated[z] ,f); }
      fputs("</scripts>\n" ,f);            
      fputs("<libs>\n" ,f);            
      for (int z=1; z <= dynlen(LibsDuplicated); ++z) {
        strreplace(LibsDuplicated[z], ",", "\t");
        strreplace(LibsDuplicated[z], ";", "\n");         
        fputs(LibsDuplicated[z] ,f); }
      fputs("</libs>\n" ,f);            
      fputs("<panels>\n" ,f);                  
      for (int z=1; z <= dynlen(PanelsDuplicated); ++z) {
        strreplace(PanelsDuplicated[z], ",", "\t");
        strreplace(PanelsDuplicated[z], ";", "\n");         
        fputs(PanelsDuplicated[z] ,f); }            
      fputs("</panels>\n" ,f);                        
      fflush(f);      
      fclose(f);

      if (SEND_EMAILS==true) {
        string pc = getHostname();
        string Head = "fwAtlas/fwAtlas_CheckProjectIntegrity.ctl - Duplicated files on project "+sysName+" on "+sFormattedTime+"\n";
        dynAppend(EmailMessage, Head);
        dynAppend(EmailMessage, "Scripts\n");        
        dynAppend(EmailMessage, ScriptsDuplicated);  
        dynAppend(EmailMessage, "Libs\n");       
        dynAppend(EmailMessage, LibsDuplicated);          
        dynAppend(EmailMessage, "Panels\n");              
        dynAppend(EmailMessage, PanelsDuplicated);                  
        emSendMail("atlasgw-exp.cern.ch", pc+".cern.ch", makeEmailContents(OnCallList, "DCS warning message", EmailMessage), iReturn);       
      }
      old_date=date;      
//  }
        
    time tTimeNow = getCurrentTime(); 
    string sFormattedTime = formatTime("%c", tTimeNow);
    date = strsplit(sFormattedTime, " ")[1]; //daily
    //date = strsplit(sFormattedTime, "/")[2]; //monthly
//    delay(60);  
//  }
}


//*****************************************************************************************************
dyn_dyn_string readFiles (dyn_string dir, string type)
{    
  dyn_dyn_string Files;
  string Path, FileExtension;
  if (type=="bin") { FileExtension = "*.*"; Path="/bin"; }  
  else if (type=="scripts") { FileExtension = "*.ctl"; Path="/scripts"; }
    else if (type=="libs") { FileExtension = "*.ctl"; Path="/scripts/libs"; }
         else { FileExtension = "*.pnl"; Path="/panels"; }
  
  for (int i=1; i <= dynlen(dir); ++i) {
        dyn_string CtrlFiles = getFileNames(dir[i]+Path, FileExtension, FILTER_FILES);
        Files[i]=CtrlFiles;    
        if (type=="scripts" || type=="libs") {
          CtrlFiles = getFileNames(dir[i]+Path, "*.lst", FILTER_FILES);
          dynAppend(Files[i],CtrlFiles);
          CtrlFiles = getFileNames(dir[i]+Path, "*.ctc", FILTER_FILES);
          dynAppend(Files[i],CtrlFiles); }
        dyn_string CtrlSubDir = getFileNames(dir[i]+Path, "*", FILTER_DIRS); 
        for (int x=1; x <= dynlen(CtrlSubDir); ++x) {
          if((CtrlSubDir[x]=="libs")||(CtrlSubDir[x]==".")||(CtrlSubDir[x]=="..")||(CtrlSubDir[x]=="./")||(CtrlSubDir[x]=="../")||(CtrlSubDir[x]==".svn")) {
            dynRemove(CtrlSubDir, x); } 
        }
        //DebugN("SUBDIR :"+CtrlSubDir);
        //dynRemove(CtrlSubDir, 1); //..  <-- this does not work, it suppresses the first dir which can be different from .. 7/2013, SZ
        for (int a=1; a <= dynlen(CtrlSubDir); ++a) {       
          if ((CtrlSubDir[a] == "..") || (CtrlSubDir[a] == ".")) continue;
          dyn_string CtrlFilesSubDir = getFileNames(dir[i]+Path+"/"+CtrlSubDir[a], FileExtension, FILTER_FILES); 
          for (int b=1; b <= dynlen(CtrlFilesSubDir); ++b) { 
            CtrlFilesSubDir[b] = CtrlSubDir[a]+"/"+CtrlFilesSubDir[b]; } //DebugN("FILESSUBDIR :"+CtrlFilesSubDir);
          dynAppend(Files[i], CtrlFilesSubDir);          
        if (type=="scripts" || type=="libs") {          
            CtrlFilesSubDir = getFileNames(dir[i]+Path+"/"+CtrlSubDir[a], "*.lst", FILTER_FILES); 
            for (int b=1; b <= dynlen(CtrlFilesSubDir); ++b) { 
              CtrlFilesSubDir[b] = CtrlSubDir[a]+"/"+CtrlFilesSubDir[b]; } //DebugN("FILESSUBDIR :"+CtrlFilesSubDir);
            dynAppend(Files[i], CtrlFilesSubDir);
            CtrlFilesSubDir = getFileNames(dir[i]+Path+"/"+CtrlSubDir[a], "*.ctc", FILTER_FILES); 
            for (int b=1; b <= dynlen(CtrlFilesSubDir); ++b) { 
              CtrlFilesSubDir[b] = CtrlSubDir[a]+"/"+CtrlFilesSubDir[b]; } //DebugN("FILESSUBDIR :"+CtrlFilesSubDir);
            dynAppend(Files[i], CtrlFilesSubDir);
          }          
        }
        //DebugN("Files on "+dir[i]+": "+Files[i]);
  }
  return Files;
}
//*****************************************************************************************************

//*****************************************************************************************************
dyn_string compareFiles(dyn_dyn_string Files, dyn_string dir, string type, dyn_string AllowedDuplicatedFiles)
{    
  dyn_string Messages;
  time tTimeNow = getCurrentTime();   
  string sFormattedTime = formatTime("%c", tTimeNow);  
  string FileExtension, Path;
  if (type=="bin") { FileExtension = "*.*"; Path="/bin"; }
  else if (type=="scripts") { FileExtension = "*.ctl"; Path="/scripts"; }
    else if (type=="libs") { FileExtension = "*.ctl"; Path="/scripts/libs"; }
         else { FileExtension = "*.pnl"; Path="/panels"; }  
  
      if(dynlen(Files)>1) {
        for (int j=1; j <= dynlen(Files)-1; ++j) {
          for (int k=j+1; k <= dynlen(Files); ++k) { //DebugN("JK "+j+""+k);  
	    //Files[j] <-> Files[k]  
            for (int l=1; l <= dynlen(Files[j]); ++l) {       
              for (int m=1; m <= dynlen(Files[k]); ++m) {  //DebugN("LM "+l+""+m);            
                if(Files[j][l]==Files[k][m]) { 
                  string File=Files[k][m]; string SubPath=""; dyn_string temp=strsplit(Files[k][m], "/"); 
                  if(dynlen(temp)>1) { 
                    SubPath=temp[1]; File=temp[2]; }
                  bool AllowDuplication = false;
                  for (int a=1; a <= dynlen(AllowedDuplicatedFiles); ++a) { //add file to the list only if not in template file                           
                    if(strpos(AllowedDuplicatedFiles[a], File) >=0) {
                      AllowDuplication = true;               
                    }
                    else { 
                    }
                  }                
                  if (AllowDuplication==false) {
                    string message = "fwAtlas_CheckProjectIntegrity - The file "+File+" of type '"+type+"' is duplicated on '"+dir[j]+Path+"/"+SubPath+"' and "+dir[k]+Path+"/"+SubPath+"'. Message sent at "+sFormattedTime;                
                    warning(message); 
                    message = File+","+dir[j]+Path+"/"+SubPath+","+dir[k]+Path+"/"+SubPath+";";                
                    dynAppend(Messages, message);                              
                  }
                }
              }              
            }
            
          }          
        }                        
        
      }
  return Messages;
}
//*****************************************************************************************************

//*****************************************************************************************************
dyn_string makeEmailContents(string to, string subject, string body, string from = "atlasdcs@cern.ch")
{
  information("Prepared SMS/E-mail with subject " + subject + " from '" + from + "' to '" + to + "'");
  return makeDynString(to, from, subject, body);
}
//*****************************************************************************************************

//*****************************************************************************************************
string getSubdetAdminList()
{
  string emails, subdet = fwAtlas_getSubdetectorId(getSystemName());
  dyn_string users, ex;
  fwAccessControl_getUsersHavingPrivilege(subdet, "Administration", users, ex);
  //DebugN("USERS :"+users);
  for (int i=1; i<=dynlen(users); ++i) {
    string user = users[i];
    emails = emails + (user+"@cern.ch;");
  }
  return emails;
}
//*****************************************************************************************************


