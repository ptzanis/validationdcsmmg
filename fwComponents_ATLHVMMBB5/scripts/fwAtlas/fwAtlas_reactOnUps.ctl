//
// Control script for UPS-on-battery signal reaction.
// The local machine will be shut down 5 minutes after
// the DSS signal has been triggered.
// 
// If the power is restored, the shutdown is cancelled
//
// Author: Stefan Schlenker, CERN
//

#uses "fwAtlas.ctl"

//
// USER library should contain valid fwAtlas_getUpsSide() function
// which returns PC location with respect to UPS power (US15 or USA15 side)
//
// create of modify
// <REPO>/ATLAS_DCS_<subdetectorID>/scripts/libs/fwAtlasUser.ctl
// using scripts/libs/fwAtlasUserTemplate.ctl as a template
//
#uses "fwAtlasUser.ctl"

int TIMEOUT = 5; // time before shutdown when running on battery
string DSS_SIGNAL_DPE_USA15 = "AL_INF_Power_UPSonBattery"; // for USA15
string DSS_SIGNAL_DPE_US15 = "AL_INF_Power_US15_UPSonBattery"; // for US15


main()
{
	fwAtlas_waitForDistConnections();

	information("fwAtlas_reactOnUps: Looking for >UPS on battery< signal for this system.");

	string side;
	if (!isFunctionDefined("fwAtlas_getUpsSide")
			|| (side=fwAtlas_getUpsSide())=="") {
		error("fwAtlas_reactOnUps: no UPS lookup defined in fwAtlasUser.ctl!"
					+" No UPS on battery reaction available!");
		return;
	}
	string upsDpe;
	if (side=="USA15") upsDpe = DSS_SIGNAL_DPE_USA15;
	else if (side=="US15") upsDpe = DSS_SIGNAL_DPE_US15;
	else {
		error("fwAtlas_reactOnUps: unknown UPS specified in fwAtlasUser.ctl!"
					+" No UPS on battery reaction available!");
		return;
	}

	upsDpe = DCS_IS_DSS+":dss_"+upsDpe+".active";
	if (!dpExists(upsDpe)) {
		error("fwAtlas_reactOnUps: UPS signal DPE "+upsDpe
					+" does not exist! No UPS on battery reaction available!");
		return;
	}

	information("fwAtlas_reactOnUps: Connecting to DSS flag >UPS on battery< "
							+upsDpe);
	dpConnect("upsOnBatteryCallback", upsDpe);

	while (1) delay(10); // for the case the script is launched from a .lst file
}

void upsOnBatteryCallback(string dpe, bool flag)
{
	if (!flag) {
		information("fwAtlas_reactOnUps: DSS flag >UPS on battery< is FALSE.");

		return;
	}

	// We are running on battery
	// Initiate shutdown within 5 minutes
	//
	warning("fwAtlas_reactOnUps: DSS flag >UPS on battery< is TRUE. Waiting for "
					+TIMEOUT+" minutes before shutting down machine");
	int countdown = TIMEOUT*60;
	while (countdown) {

		// if UPS not anymore on battery, cancel shutdown
		//
		bool dssFlag;
		dpGet(dpe, dssFlag);
		if (!dssFlag) {
			information("fwAtlas_reactOnUps: Shutdown cancelled.");
			return;
		}

		warning("System runs on UPS battery! Shutdown will commence in "+countdown
						+" seconds!");
		--countdown;
		delay(1);
	}
	information("System runs on UPS battery! System will be shutdown NOW.");
	if (_WIN32) {
		DebugTN("SHUTDOWN /L /T:0 /Y /C");
		system("SHUTDOWN /L /T:0 /Y /C"); // shutdown windows
// 		DebugTN(system("SHUTDOWN /R /L /T:0 /Y /C")); // reboot for testing
	}
	else {
		system("/sbin/shutdown -h 0"); // halt linux
	}
}
