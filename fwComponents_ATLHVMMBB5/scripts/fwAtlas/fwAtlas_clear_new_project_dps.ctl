main()
{
  dyn_string dpts = makeDynString("ANALOG1", "ANALOG2", "BIT_CONDITION", "COUNTER1", "COUNTER_SUB", "DRIVE1", "DRIVE2",
                                  "ExampleDP_BarTrend", "ExampleDP_Bit", "ExampleDP_DDE", "ExampleDP_Float", "ExampleDP_Int", "ExampleDP_Text",
                                  "LABOR_ANALOG", "LABOR_COUNTER", "MODE_CMD", "MODE_STATE", "PUMP1", "PUMP2", "SETPOINT",
                                  "SLIDE_VALVE1", "SLIDE_VALVE2", "SLIDE_VALVE_HAND1", "SSC_KPI_Instance", "WH_SC1", "WH_SC_SERVICE", "WH_SC_SUB");
  dyn_string dps;
  for (int i=1;i<=dynlen(dpts);++i) {
    string dpt = dpts[i];
    dynAppend(dps, dpNames("*",dpt));
  }
  DebugTN(dps, dpts);
  for (int i=1;i<=dynlen(dps);++i) { 
    dpDelete(dps[i]);
  }
  for (int i=1;i<=dynlen(dpts);++i) {
    dpTypeDelete(dpts[i]);
  }
  
  // Removing useless archiving. Introduced 18.04.16 by SK
  dyn_string exceptionInfo;
  dyn_string targetDpes = dpNames("_Pager*.statusMessage", "_Pager");
  dynAppend(targetDpes, dpNames("_ReduManager*.", "_ReduManager"));
  dynAppend(targetDpes, dpNames("_ReduManager*.MissingMonitoredManagers", "_ReduManager"));
  DebugTN(dynlen(targetDpes)+" DPE found with useless archiving:"+targetDpes);
//return;
  for(int i=1; i<=dynlen(targetDpes); i++) {
    fwArchive_deleteMultiple(makeDynString(targetDpes[i]), exceptionInfo);
    if(dynlen(exceptionInfo))
      DebugTN("Exception Info: "+exceptionInfo);
  }

  // add UI dps and remove archiving on existing ones
  //
  dyn_string ex;
  for (int i=1;i<=10;++i) {
    fwArchive_delete("_Ui_"+i+".UserName", ex);
  }
  for (int i=11;i<=20;++i) {
    dpCreate("_Ui_"+i, "_Ui");
  }
}
