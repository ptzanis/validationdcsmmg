#uses "fwInstallationDBAgent.ctl"
#uses "fwInstallationDB.ctl"
main()
{
  string database,username,password,owner;
  int  restartProject = 0;
  dpGet("fwInstallation_agentParametrization.db.connection.server", database,
    "fwInstallation_agentParametrization.db.connection.username", username,
    "fwInstallation_agentParametrization.db.connection.schemaOwner", owner,
    "fwInstallation_agentParametrization.db.connection.password", password); 
  DebugTN("Connection ",database,username); 
 
  if(database != "" && username != "" && password != ""){
    fwInstallationDB_setUseDB(true);
    int con = fwInstallationDB_openDBConnection(database, username, password, owner);
    if (con >= 0) {
      int restartProject = 0;
      int projectId, autoregEnabled;
      fwInstallationDB_isProjectRegistered(projectId);
      fwInstallationDB_getProjectAutoregistration(autoregEnabled);
      if (projectId > 0 )
        DebugTN("Project has been registered already");    
      else {
        if ( autoregEnabled == 1) //if the project is already registered or the autoregistration is enabled
        {  
          if(fwInstallationDBAgent_synchronize(restartProject)) {fwInstallation_throw("Failed to upate the System Configuration DB after component deletion", "WARNING", 10); return;}
        }
        else DebugTN("Autoregister is not allowed");
      }
    }
    else DebugTN("No Connection"+con);
  }
  else DebugTN("Connection parameters empty");
  
   int cl =  fwInstallationDB_closeDBConnection(); 
}
